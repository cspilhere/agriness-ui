const webpack = require('karma-webpack');

module.exports = (config) => {
  config.set({

    basePath: '',

    frameworks: ['jasmine'],

    files: [
      'https://code.angularjs.org/1.6.6/angular.js',
      'https://code.angularjs.org/1.6.6/angular-mocks.js',
      './dev/app/**/**/**/**/*spec.js'
    ],

    preprocessors: {
      './dev/app/**/**/**/**/*spec.js': ['webpack'],
      './dev/app/**/**/*.html': 'ng-html2js'
    },

    ngHtml2JsPreprocessor: {
      moduleName: 'support-html',
    },

    plugins: [
      webpack,
      'karma-jasmine',
      'karma-chrome-launcher',
      'karma-htmlfile-reporter',
      'karma-ng-html2js-preprocessor'
    ],

    colors: true,

    browsers: ['Chrome_without_security'],

    customLaunchers: {
      Chrome_without_security: {
        base: 'Chrome',
        flags: ['--disable-web-security']
      }
    },

    htmlReporter: {
      outputFile: 'reports/report.html',
      pageTitle: 'Agriness S3 Air',
      subPageTitle: 'Unit tests'
    },

    singleRun: true
  })
};
