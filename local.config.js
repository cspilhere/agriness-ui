module.exports = {
  port: '8080',
  host: 'http://localhost',
  directories: {
    temp: '.tmp',
    dev: 'dev',
    prod: 'build'
  }
}
