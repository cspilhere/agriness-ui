const path = require('path');
const webpack = require('webpack');

const localconfig = require('./local.config.js');

module.exports = {
  entry: {
    bundle: __dirname + '/' + localconfig.directories.dev + '/app/entry.js',
    'components': __dirname + '/' + localconfig.directories.dev + '/app/components'
  },
	output: {
		path: path.join(__dirname, localconfig.directories.prod),
    publicPath: localconfig.directories.prod + '/',
    filename: 'app/[name].js',
	},
  module: {
    rules: [{
      test: /\.(js)$/,
      exclude: /node_modules/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: ['env'],
          cacheDirectory: true
        }
      }
    }, {
      enforce: 'post',
      test: /\.js$/,
      exclude: /\/(node_modules)\//,
      loader: 'autopolyfiller-loader',
      options: {
        browsers: ['last 3 versions', 'ie >= 9', 'iOS 7'],
        include: ['Object.assign', 'Array.from', 'Promise']
      }
    }]
  },
	resolve: {
    unsafeCache: true,
    alias: {
      app: path.join(__dirname, './dev/app/app.js'),
      utils: path.join(__dirname, './dev/app/utils'),
      core: path.join(__dirname, './dev/app/core'),
      config: path.join(__dirname, './dev/app/config'),
      core: path.join(__dirname, './dev/app/core'),
      directives: path.join(__dirname, './dev/app/directives'),
      components: path.join(__dirname, './dev/app/components'),
      features: path.join(__dirname, './dev/app/features'),
      routes: path.join(__dirname, './dev/app/routes'),
      services: path.join(__dirname, './dev/app/services')
    }
	},
	plugins: []
};
