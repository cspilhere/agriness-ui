const gulp = require('gulp');
const cachebust = require('gulp-cache-bust');

const localconfig = require('../local.config.js');
 
gulp.task('cachebust:build', () => {
	setTimeout(() => {
		gulp.src('./' + localconfig.directories.prod + '/index.html')
		.pipe(cachebust({ type: 'timestamp' }))
		.pipe(gulp.dest('./' + localconfig.directories.prod + '/'));
	}, 100);
});
