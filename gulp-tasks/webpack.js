const gulp = require('gulp');
const gutil = require('gulp-util');
const connect = require('gulp-connect');
const plumber = require('gulp-plumber');

const webpack = require('webpack');
const webpackStream = require('webpack-stream');

const webpackConfig = require('../webpack.config.js');
const webpackConfigBuild = require('../webpack.config.build.js');

const localconfig = require('../local.config.js');

gulp.task('webpack', (callback) => {
	return gulp.src(localconfig.directories.dev + '/app/entry.js')
	.pipe(plumber())
	.pipe(webpackStream(webpackConfig, webpack))
	.pipe(gulp.dest(localconfig.directories.temp))
	.pipe(connect.reload());
});

// gulp.task('webpack', (callback) => {
// 	const devCompiler = webpack(Object.create(webpackConfig));
// 	devCompiler.run(function(err, stats) {
// 		if (err) throw new gutil.PluginError('webpack', err);
// 		gutil.log('[webpack]', stats.toString({ colors: true }));
// 		callback();
// 	});
// });

gulp.task('webpack:build', (callback) => {
	const devCompiler = webpack(Object.create(webpackConfigBuild));
	devCompiler.run(function(err, stats) {
		if (err) throw new gutil.PluginError('webpack:build', err);
		gutil.log('[webpack:build]', stats.toString({ colors: true }));
		callback();
	});
});
