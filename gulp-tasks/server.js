const gulp = require('gulp');
const opn = require('opn');
const connect = require('gulp-connect');

const localconfig = require('../local.config.js');

gulp.task('server', () => {
  connect.server({
    root: [
      './' + localconfig.directories.temp,
      './' + localconfig.directories.dev
    ],
    port: localconfig.port,
    livereload: true,
    middleware: (connect, opt) => []
  });
  opn(localconfig.host + ':' + localconfig.port);
});

gulp.task('server:build', () => {
  connect.server({
    root: [
      './' + localconfig.directories.prod
    ],
    port: localconfig.port,
    livereload: true,
    middleware: (connect, opt) => []
  });
  opn(localconfig.host + ':' + localconfig.port);
});
