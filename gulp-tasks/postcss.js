const gulp = require('gulp');
const connect = require('gulp-connect');

const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const flexbugs = require('postcss-flexbugs-fixes');

const localconfig = require('../local.config.js');

const postcssPlugins = [
  autoprefixer({ browsers: ['last 3 versions', 'ie >= 9', 'ios_saf >= 7'] }),
  cssnano(),
  flexbugs()
];

gulp.task('postcss', function () {
  setTimeout(() => {
    return gulp.src('./' + localconfig.directories.temp + '/styles/**/*.css')
    .pipe(postcss(postcssPlugins))
    .pipe(gulp.dest('./' + localconfig.directories.temp + '/styles/'))
    .pipe(connect.reload());
  }, 100);
});

gulp.task('postcss:build', function () {
  setTimeout(() => {
    return gulp.src('./' + localconfig.directories.prod + '/styles/**/*.css')
    .pipe(postcss(postcssPlugins))
    .pipe(gulp.dest('./' + localconfig.directories.prod + '/styles/'))
  }, 100);
});
