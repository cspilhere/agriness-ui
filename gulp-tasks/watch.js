const gulp = require('gulp');
const connect = require('gulp-connect');

const watch = require('gulp-watch');
const wait = require('gulp-wait');
const localconfig = require('../local.config.js');

gulp.task('webpack-reload', () => {
  gulp.src([localconfig.directories.temp + '/app/bundle.js']).pipe(wait(400)).pipe(connect.reload());
});

gulp.task('watch', () => {
  gulp.watch([localconfig.directories.dev + '/**/**/*.html'], ['webpack-reload']);
  gulp.watch([localconfig.directories.dev + '/styles/**/*.scss'], ['sass']);
  gulp.watch(['./app/**/**/*.js'], ['webpack']);
});
