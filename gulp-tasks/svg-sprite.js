const gulp = require('gulp');
const run = require('run-sequence');
const plumber = require('gulp-plumber');

const svgSprite = require('gulp-svg-sprite');

const localconfig = require('../local.config.js');

gulp.task('svg-sprite', () => {
  gulp.src('./' + localconfig.directories.dev + '/media/icons/**/*.svg')
  .pipe(plumber())
  .pipe(svgSprite({
    shape: {
      dimension: {
        maxWidth: 32,
        maxHeight: 32,
        precision: 2,
        attributes: false,
      },
      spacing: {
        padding: 0,
        box: 'content'
      },
      transform: [{
        svgo: {
          plugins : [{
            removeAttrs: {
              attrs: ['fill', 'fill-rule', 'stroke']
            }
          }]
        }
      }]
    },
    mode: {
      defs: {
        bust: false,
        sprite: 'sprites/sprite.defs.svg',
        dest: '',
        example: {
          dest: 'sprites/sprite.svg.defs.html'
        },
      }
    },
    svg: {
      xmlDeclaration: true,
      doctypeDeclaration: true,
      namespaceIDs: true,
      dimensionAttributes: true
    },
    variables: {}
  }))
  .pipe(gulp.dest('./' + localconfig.directories.temp + '/media/'));
});

gulp.task('svg-sprite:build', () => {
  gulp.src('./' + localconfig.directories.dev + '/media/icons/**/*.svg')
  .pipe(plumber())
  .pipe(svgSprite({
    shape: {
      dimension: {
        maxWidth: 32,
        maxHeight: 32,
        precision: 2,
        attributes: false,
      },
      spacing: {
        padding: 0,
        box: 'content'
      },
      transform: [{
        svgo: {
          plugins : [{
            removeAttrs: {
              attrs: ['fill', 'fill-rule', 'stroke']
            }
          }]
        }
      }]
    },
    mode: {
      defs: {
        bust: false,
        sprite: 'sprites/sprite.defs.svg',
        dest: '',
        example: {
          dest: 'sprites/sprite.svg.defs.html'
        },
      }
    },
    svg: {
      xmlDeclaration: true,
      doctypeDeclaration: true,
      namespaceIDs: true,
      dimensionAttributes: true
    },
    variables: {}
  }))
  .pipe(gulp.dest('./' + localconfig.directories.prod + '/media/'));
});
