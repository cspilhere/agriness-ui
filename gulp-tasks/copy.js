const gulp = require('gulp');
const connect = require('gulp-connect');

const localconfig = require('../local.config.js');

gulp.task('copy:build', () => {
  return gulp.src([
    '!' + localconfig.directories.dev + '/media/icons/*.*',
    '!' + localconfig.directories.dev + '/media/sprites/*.*',
    '!' + localconfig.directories.dev + '/app/**/**/*.js',
    '!' + localconfig.directories.dev + '/styles/**/**/*.*',
    localconfig.directories.dev + '/**/**/*.*'
  ])
  .pipe(gulp.dest(localconfig.directories.prod));
});

gulp.task('copy:post-build-js', () => {
  return gulp.src([
    localconfig.directories.prod + '/app/components.js'
  ])
  .pipe(gulp.dest(localconfig.directories.prod + '/'));
});

gulp.task('copy:post-build-css', () => {
  return gulp.src([
    localconfig.directories.prod + '/styles/styles.css'
  ])
  .pipe(gulp.dest(localconfig.directories.prod + '/'));
});
