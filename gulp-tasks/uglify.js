const gulp = require('gulp');
const uglify = require('gulp-uglify');

const localconfig = require('../local.config.js');

gulp.task('uglify:build', () => {
  gulp.src('./' + localconfig.directories.prod + '/scripts/bundle.js')
  .pipe(uglify({
    mangle: true,
    compress: true
  }))
  .pipe(gulp.dest('./' + localconfig.directories.prod + '/scripts'));
});
