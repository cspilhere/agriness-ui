const gulp = require('gulp');
const connect = require('gulp-connect');
const sass = require('gulp-sass');
const plumber = require('gulp-plumber');
const sassGlob = require('gulp-sass-glob-import');

const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const flexbugs = require('postcss-flexbugs-fixes');

const rename = require('gulp-rename');

const localconfig = require('../local.config.js');
const filename = 'styles.css';

const postcssPlugins = [
  autoprefixer({ browsers: ['last 3 versions', 'ie >= 9', 'ios_saf >= 7'] }),
  cssnano(),
  flexbugs()
];

gulp.task('sass', () => {
  return gulp.src('./' + localconfig.directories.dev + '/styles/main.scss')
  .pipe(plumber())
  .pipe(sassGlob())
  .pipe(sass())
  .pipe(rename(filename))
  .pipe(postcss(postcssPlugins))
  .pipe(gulp.dest('./' + localconfig.directories.temp + '/styles/'))
  .pipe(connect.reload());
});

gulp.task('sass:build', () => {
  return gulp.src('./' + localconfig.directories.dev + '/styles/main.scss')
  .pipe(plumber())
  .pipe(sassGlob())
  .pipe(sass())
  .pipe(rename(filename))
  .pipe(postcss(postcssPlugins))
  .pipe(gulp.dest('./' + localconfig.directories.prod + '/styles/'))
  .pipe(connect.reload());
});
