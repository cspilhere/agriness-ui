const gulp = require('gulp');
const del = require('del');

const localconfig = require('../local.config.js');

gulp.task('clean', () => {
  del(localconfig.directories.temp, { force: true });
  del(localconfig.directories.prod, { force: true });
});
