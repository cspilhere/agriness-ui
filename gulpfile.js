const gulp = require('gulp');
const run = require('run-sequence');

require('require-dir')('./gulp-tasks');

gulp.task('default', () => {
  run('clean');
  setTimeout(() => run(
    'server',
    'watch',
    'sass',
    'svg-sprite',
    'webpack'
  ), 100);
});

gulp.task('build', () => {
  run('clean');
  setTimeout(() => run(
    'sass:build',
    'cachebust:build',
    'svg-sprite:build',
    'webpack:build',
    'copy:build',
    'copy:post-build-js',
    'copy:post-build-css'
  ), 1000);
});
