import app from 'app';

app.controller('tabsController', ($scope) => {

  $scope.content = 'tab content';

  $scope.tableModel = [{
    "holding_user": [{
      "user_uuid": "8c4cd678-85b8-4389-9d15-17c11a7a6441",
      "relation_type": "OWNER"
    }],
    "created_date": "2017-09-28T12:10:40.473870+00:00",
    "type": "RURAL PROPERTY",
    "status": "ACTIVE HOLDING",
    "name": "Holding 42397",
    "uuid": "452922c9-3a17-4fa0-9563-62aeb5c95037",
    "id": 1
  }, {
    "holding_user": [{
      "user_uuid": "8c4cd678-85b8-4389-9d15-17c11a7a6441",
      "relation_type": "OWNER"
    }],
    "created_date": "2017-09-28T12:11:05.422443+00:00",
    "type": "RURAL PROPERTY",
    "status": "ACTIVE HOLDING",
    "name": "Holding 75267",
    "uuid": "08025a58-21c7-4c71-8e9d-97aa8893fcac",
    "id": 2
  }];

});
