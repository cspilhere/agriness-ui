export default [{
  name: 'A granja',
  icon: 'incluir2',
  path: '',
  params: null,
  profile: '1,2,3,4',
  children: [{
    name: 'Visão Geral',
    path: null,
    params: null,
    profile: '1,2,3,4'
  }, {
    name: 'Análises',
    path: '',
    params: null,
    profile: '1,2,3,4'
  }, {
    name: 'Auditoria',
    path: '',
    params: null,
    profile: '1,2,3,4'
  }, {
    name: 'Configurações',
    path: '',
    params: null,
    profile: '1,2,3,4'
  }],
  apps: [{
    name: 'Configurações',
    path: '',
    profile: '1,2,3,4'
  }, {
    name: 'Auditoria',
    path: '',
    profile: '1,2,3,4'
  }, {
    name: 'Análises',
    path: '',
    profile: '1,2,3,4'
  }]
}, {
  name: 'Pensamento +1',
  icon: 'incluir2',
  path: '',
  params: null,
  profile: '1,2,3,4',
  children: [{
    name: 'Mapa da produção',
    path: null,
    params: null,
    profile: '1,2,3,4'
  }, {
    name: 'Árvore de Diagnóstico',
    path: '',
    params: null,
    profile: '1,2,3,4'
  }, {
    name: 'Reuniões Semanais',
    path: null,
    params: null,
    profile: '1,2,3,4'
  }, {
    name: 'Imersão p! (para alunos)',
    path: null,
    params: null,
    profile: '1,2,3,4'
  }],
  apps: []
}, {
  name: 'Insumos',
  icon: 'consumos',
  path: '',
  params: null,
  profile: '1,2,3,4',
  children: [{
    name: 'Consumos',
    path: '',
    params: null,
    profile: '1,2,3,4'
  }],
  apps: [{
    name: 'Consumos',
    path: '',
    profile: '1,2,3,4'
  }]
}, {
  name: 'Financeiro',
  icon: 'rotulos',
  path: null,
  params: null,
  profile: '1,2,3,4',
  children: [{
    name: 'Contas a pagar',
    path: null,
    params: null,
    profile: '1,2,3,4'
  }, {
    name: 'Contas a receber',
    path: null,
    params: null,
    profile: '1,2,3,4'
  }, {
    name: 'Custo de Produção',
    path: null,
    params: null,
    profile: '1,2,3,4'
  }]
}];
