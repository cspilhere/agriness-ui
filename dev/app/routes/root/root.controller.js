import constants from 'core/app.constants';
import appHeaderTree from './config/appHeaderTree';

export default ($scope, $timeout) => {

  $scope.appIsLoaded = false;
  $timeout(() => $scope.appIsLoaded = true, 3000);

  $scope.appHeaderData = appHeaderTree;

  $scope.userData = {
    name: 'Senhor do Mar',
    email: 'domar@gmail.com',
    picture: 'https://www.gravatar.com/avatar/00000000000000000000000000000000?d=retro&f=y'
  };

  $scope.logout = () => {
    console.log('logout o/');
  };

  $scope.clickSidebar = (link) => {
    console.log(link);
  };

  $scope.inlineNavigationLinks = [{
    name: 'Saudi Arabia',
    value: 'Saudi Arabia',
    checked: true
  }, {
    name: 'Myanmar (Burma)',
    value: 'Myanmar (Burma)',
    checked: false
  }, {
    name: 'Grenada',
    value: 'Grenada',
    checked: false
  }, {
    name: 'Cameroon',
    value: 'Cameroon',
    checked: false
  }];

  $scope.clickInlineNavigation = (item) => {
    console.log(item);
  };

  $scope.cardList = [{
    title: 'Moldova',
    description: '599 Rukgo Path',
    situation: null
  }, {
    title: 'Wallis & Futuna',
    description: '832 Hemga Heights',
    situation: 'bought'
  }, {
    title: 'Nicaragua',
    description: '647 Evsik Park Teeb Pass',
    situation: null
  }, {
    title: 'Romania',
    description: '1483 Litfa View',
    situation: null
  }, {
    title: 'Djibouti',
    description: '1139 Jamodu Point',
    situation: null
  }, {
    title: 'Guinea',
    description: '1413 Imro Loop',
    situation: 'bought'
  }, {
    title: 'Cameroon',
    description: '1031 Teeb Pass',
    situation: 'bought'
  }];

  $scope.cardClickSecondary = (callbackData) => {
    console.log(callbackData);
  };


  $scope.openModal = false;
  // $scope.cardClickPrimary = (callbackData) => {
  //   console.log(callbackData);
  //   $modal.open({
  //     title: 'Opa!',
  //     hideCloseButton: true
  //   });
  //   setTimeout(() => {
  //     $modal.block('error', {
  //       statusMessage: 'Deu ruim... :(',
  //       onLeave: (foo) => {
  //         $modal.close();
  //       },
  //       onStay: (foo) => {
  //         $modal.unBlock();
  //         setTimeout(() => {
  //           $modal.block('success', {
  //             statusMessage: 'Sucesso!'
  //           });
  //         }, 1000);
  //       }
  //     });
  //   }, 1000);
  // };

  $scope.onCloseModal = () => {
    $scope.openModal = false;
  };


  $scope.alertIsOpen = false;

  $scope.onCloseAlert = () => {
    $scope.alertIsOpen = false;
    console.log('Alert');
  };

  $scope.openAlert = () => {
    console.log('asas');
    $scope.alertIsOpen = true;
  };

};
