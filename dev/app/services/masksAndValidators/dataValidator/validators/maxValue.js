// import Format from '../../../Format';
export default function maxValue(value, max) {
  // value = Format().number(value);
  if (!value && value != 0) return false;
  if ((value * 1) > (max * 1)) return false;
  return true;
} // maxValue
