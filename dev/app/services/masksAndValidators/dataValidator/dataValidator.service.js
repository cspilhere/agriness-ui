import cpf from './validators/cpf';
import cnpj from './validators/cnpj';
import maxLength from './validators/maxLength';
import maxValue from './validators/maxValue';
import minLength from './validators/minLength';
import minValue from './validators/minValue';
import onlyLetters from './validators/onlyLetters';
import onlyNumbers from './validators/onlyNumbers';
import range from './validators/range';
import email from './validators/email';
import decimals from './validators/decimals';
// import dateRange from './validators/dateRange';

import number from './validators/number';

function DataValidator() {

  return {
    cpf: cpf,
    cnpj: cnpj,
    maxLength: maxLength,
    minLength: minLength,
    maxValue: maxValue,
    minValue: minValue,
    onlyLetters: onlyLetters,
    onlyNumbers: onlyNumbers,
    range: range,
    email: email,
    decimals: decimals,
    // dateRange: dateRange,
    number: number
  };

} // DataValidator

export default DataValidator;
