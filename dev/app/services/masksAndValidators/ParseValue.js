function ParseValue(Mask, Validator) {

  class ParseValue {

    constructor(Mask, Validator) {
      this.Mask = Mask;
      this.Validator = Validator;
    } // constructor()

    parse(value, maskArray, validatorArray) {

      let extractParams = /\((.*?)\)/;
      let extractString = /^\w+/g;
      let masks = [];
      let validators = [];

      let validations = [];
      let checkValidations = () => validations.filter((item) => item === false).length > 0 ? false : true;

      let newValue = {};

      newValue.originalValue = value;
      newValue.parsedValue = value;
      newValue.isValid = false;

      if (maskArray) {

        maskArray.split(';').forEach((item) => {
          if (!item) return;
          let temp = {};
          item = item.trim();
          temp.maskType = item.match(extractString)[0];
          temp.params = item.match(extractParams) ? item.match(extractParams)[1] : null;
          masks.push(temp);
        });

        masks.forEach((mask, index) => {
          let allParams = [];
          let params = mask.params ? mask.params.split(',') : [];
          allParams = allParams.concat(params);
          allParams.unshift(newValue.parsedValue);
          if (Mask[mask.maskType] && Mask[mask.maskType].apply(null, allParams) !== false) {
            if (Mask[mask.maskType].apply(null, allParams) === '') {
              newValue.parsedValue = Mask[mask.maskType].apply(null, allParams);
            } else {
              newValue.parsedValue = Mask[mask.maskType].apply(null, allParams) || newValue.originalValue;
            }
          } else {
            newValue.parsedValue = newValue.originalValue;
          }
        });

      }

      if (validatorArray) {

        validatorArray.split(';').forEach((item) => {
          let temp = {};
          item = item.trim();
          if (!item) return;
          temp.validatorType = item.match(extractString)[0];
          temp.params = item.match(extractParams) ? item.match(extractParams)[1] : null;
          validators.push(temp);
        });

        validators.forEach((validator, index) => {
          if (!Validator[validator.validatorType]) return;
          let allParams = [];
          let params = validator.params ? validator.params.split(',') : [];
          params = params.map((param) => param.trim());
          allParams = allParams.concat(params);
          allParams.unshift(newValue.parsedValue);
          validations.push(Validator[validator.validatorType].apply(null, allParams));
        });

      }

      newValue.isValid = checkValidations();

      return newValue;

    }

  }

  return new ParseValue(Mask, Validator).parse;

} // function ParseValue

export default ParseValue;
