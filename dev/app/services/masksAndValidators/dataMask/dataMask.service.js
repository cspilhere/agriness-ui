import camelCaseToDash from './masks/camelCaseToDash';
import capitalize from './masks/capitalize';
import cep from './masks/cep';
import cnpj from './masks/cnpj';
import cpf from './masks/cpf';
import date from './masks/date';
// import datetime from './masks/datetime';
import time from './masks/time';
import maxLength from './masks/maxLength';
import onlyLetters from './masks/onlyLetters';
import onlyNumbers from './masks/onlyNumbers';
import trim from './masks/trim';
import decimals from './masks/decimals';
import decimals2 from './masks/decimals2';
import decimals3 from './masks/decimals3';
import decimals_teste from './masks/decimals_teste';
import currency from './masks/currency';
import currency2 from './masks/currency2';
import sliceNumber from './masks/sliceNumber';

import number from './masks/number';


function DataMask() {

  return {
    camelCaseToDash: camelCaseToDash,
    capitalize: capitalize,
    cep: cep,
    cnpj: cnpj,
    cpf: cpf,
    date: date,
    // datetime: datetime,
    time: time,
    maxLength: maxLength,
    onlyLetters: onlyLetters,
    onlyNumbers: onlyNumbers,
    trim: trim,
    decimals: decimals,
    decimals2: decimals2,
    decimals3: decimals3,
    decimals_teste: decimals_teste, // Temporario para metas e validacoes
    currency: currency,
    currency2: currency2,
    sliceNumber: sliceNumber,
    number: number
  };

} // DataMask

export default DataMask;
