
export default function datetime(date, format) {

  if (!date) return '';

  let splitedDatetime = date.split(' ');

  date = splitedDatetime[0];

  if (format) {

    if (format === 'yyyy-MM-dd HH:mm') {
      format = format.split(' ');
      format = format[0];
      date = date.toString();
      date = date.replace(/\D/g, '');
      date = date.replace(/[^\dA-Z]+/g, '');
      if (date.length > 4) date = date.substring(0, 4) + '-' + date.substring(4);
      if (date.length > 7) date = date.substring(0, 7) + '-' + date.substring(7, 9);
			if(splitedDatetime[1]) {
				return date + ' ' + splitedDatetime[1];
			} else {
				return date;
			}
    }


    // PT-BR Format
    if (format === 'dd/MM/yyyy HH:mm') {
      format = format.split(' ');
      format = format[0];
      if (/^((\d{4})-(\d{2})-(\d{2}))$/.test(date)) {
        let values = date.split('-');
        if (date.length > 2) date = date.substring(0, 2) + '/' + date.substring(2);
        if (date.length > 5) date = date.substring(0, 5) + '/' + date.substring(5, 9);
        date = values[2] + '/' + values[1] + '/' + values[0];
				if(splitedDatetime[1]) {
					return date + ' ' + splitedDatetime[1];
				} else {
					return date;
				}
      }
      date = date.toString();
      date = date.replace(/\D/g, '');
      date = date.replace(/[^\dA-Z]+/g, '');
      if (date.length > 2) date = date.substring(0, 2) + '/' + date.substring(2);
      if (date.length > 5) date = date.substring(0, 5) + '/' + date.substring(5, 9);
			if(splitedDatetime[1]) {
				return date + ' ' + splitedDatetime[1];
			} else {
				return date;
			}
    }

  }

  if (/^((\d{4})-(\d{2})-(\d{2}))$/.test(date)) {
    let values = date.split('-');
    if (date.length > 2) date = date.substring(0, 2) + '/' + date.substring(2);
    if (date.length > 5) date = date.substring(0, 5) + '/' + date.substring(5, 9);
    return values[2] + '/' + values[1] + '/' + values[0];
  }
  date = date.toString();
  date = date.replace(/\D/g, '');
  date = date.replace(/[^\dA-Z]+/g, '');
  if (date.length > 2) date = date.substring(0, 2) + '/' + date.substring(2);
  if (date.length > 5) date = date.substring(0, 5) + '/' + date.substring(5, 9);
	if(splitedDatetime[1]) {
		return date + ' ' + splitedDatetime[1];
	} else {
		return date;
	}

} // function datetime
