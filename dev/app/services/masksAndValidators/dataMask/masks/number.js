export default function number(number, float, slice) {

  if (!number) return;
  float = float * 1;
  float = float > 0 ? float + 1 : float;
  
  number = number + '';

  let thousandsRegex = new RegExp('([0-9])(?=([0-9]{3})+(?![0-9]))', 'g');
  let decimalsRegex = new RegExp(`((\,)[0-9]{${float},})$`, 'g');

  let decimalPart = '';
  let intPart = number;

  decimalPart = number.match(/((\,)$){1}|((\,)[0-9]{1,}$){1}/g);
  decimalPart = (decimalPart ? decimalPart[0] : '');

  intPart = number.replace(/(^([/d|/D])+){0,}(?:((\,)|(\,)[0-9]{1,})$)/g, '$1');

  if (decimalPart.match(decimalsRegex)) decimalPart = decimalPart.slice(0, -1);

  intPart = intPart.replace(/(\D)/g, '');

  intPart = intPart.replace(thousandsRegex, '$1' + '.');

  number = intPart + decimalPart;

  return number;

} // number
