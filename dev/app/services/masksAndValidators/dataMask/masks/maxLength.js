export default function maxLenght(data, maxLenght) {
  if (!data) return;
  return data.toString().substring(0, maxLenght);
} // maxLenght
