export default function decimals(number, decimalsLength = 2, separator) {

  if (!number) return '';

  let numberString = (number + '');

  if (numberString.match(/\.[0-9]{5}$/g)) {
    return numberString;
  }

  let regex = new RegExp(`([0-9]{2})$`, 'g');

  numberString = numberString.replace(/\D/g, '');
  numberString = numberString.replace(/^(0)+/, '');

  function zeros() {
    zeros = '';
    let l = 3 - numberString.length;
    for (let i = 0; i < l; i++) zeros += '0';
    return zeros;
  }

  numberString = zeros() + numberString;

  numberString = numberString.replace(regex, (separator || '.') + '$1');

  return numberString;

} // decimals

/**
 * Presentation mask for decimals
 * TODO: Implement others unities
 */

// export default function decimals(number, decimals = 2, decimalsSep = ',') {

//   // if (!number) return '';

//   number = number + '';

//   let formatedNumber = number;

//   formatedNumber = formatedNumber.replace(',', '.');

//   let decimalsPart = formatedNumber.match(/(\,|\.[0-9]+)$/g);

//   console.log(decimalsPart);

//   // trim decimals part
//   if (decimalsPart) {
//     decimalsPart = decimalsPart;
//   }


//   // let clearSeparators = (value) => {
//   //   value = value.toString();
//   //   return parseFloat(value.replace(/,/g, '.').replace(/\.(?![^.]*$)/g, ''));
//   // }

//   // let toIntCents = (number) => {
//   //   return Math.abs(parseInt(clearSeparators(number) * 100));
//   // };

//   // if (separator !== '.,' && separator !== ',.') separator = '.,';

//   // function formatDecimal(number, separator) {
//   //   let value = number + '';
//   //   let regex = new RegExp(`([0-9])([0-9]{${decimals == null ? 2 : decimals}})$`);
//   //   value = value.replace(/\D/g,'');
//   //   value = value.replace(/([0-9])([0-9]{14})$/, '$1' + separator[0] + '$2');
//   //   value = value.replace(/([0-9])([0-9]{11})$/, '$1' + separator[0] + '$2');
//   //   value = value.replace(/([0-9])([0-9]{8})$/, '$1' + separator[0] + '$2');
//   //   value = value.replace(/([0-9])([0-9]{5})$/, '$1' + separator[0] + '$2');
//   //   value = value.replace(regex, '$1' + separator[1] + '$2');
//   //   return value;
//   // }

//   // if (decimals < 1 && decimals !== null) {
//   //   return parseInt(number);
//   // };

//   // if (unity) {
//   //   unity = unity.toString();
//   //   unity = unity.toLowerCase();
//   //   if (unity === 'kilogram' || unity === 'kg') return formatDecimal(toIntCents(number), separator) + ' KG';
//   // }

//   // if (number.match('0.')) {
//   //   return (number * 1).toFixed(decimals || 2).replace(separator[0], separator[1]);
//   // }

//   return formatedNumber;

// };

// console.log(decimals('100,0000', null, ','));
// console.log(decimals('1.000,00', null, ','));
// console.log(decimals('100.00', null));
// console.log(decimals('1,000.00', null));
// console.log(decimals('1000', null));
// console.log(decimals('1000.190', null));
