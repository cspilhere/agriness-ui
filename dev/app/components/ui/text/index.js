/**
 * @namespace @directive [ui] text
 */

import 'angular';

angular.module('text', [])
.directive('text', directive)
.run(($templateCache) => (
  $templateCache.put('components/ui/text', `
    <p class="text" ng-class="{
      'text--small': vm.size == 'small',
      'text--medium': vm.size == 'medium',
      'text--large': vm.size == 'large'
    }"><ng-transclude></ng-transclude></p>
  `)
));

function directive($templateCache) {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    scope: {
      size: '@'
    },
    controller: controller,
    controllerAs: 'vm',
    bindToController: true,
    template: $templateCache.get('components/ui/text')
  };
};

function controller($scope) {};
