import './';

describe('labelComponent basic tests', () => {

  let $compile, $rootScope;

  beforeEach(angular.mock.module('labelComponent'));

  beforeEach(angular.mock.inject((_$compile_, _$rootScope_) => {
    $compile = _$compile_;
    $rootScope = _$rootScope_;
  }));

  it('Renderiza o label padrão.', () => {
    let element = $compile(`<label-component>Padrão</label-component>`)($rootScope);
    $rootScope.$digest();
    expect(element[0].textContent).toContain('Padrão');
    expect(element[0].classList.contains('label--secondary')).toBe(false);
  });

  it('Renderiza o label com cor customizada.', () => {
    let element = $compile(`<label-component color="rgb(102, 102, 102)">Cor</label-component>`)($rootScope);
    $rootScope.$digest();
    expect(element[0].style.backgroundColor).toBe('rgb(102, 102, 102)');
  });

  it('Renderiza o label com o botão de fechar.', () => {
    $rootScope.callback = () => {};
    let element = $compile(`
      <label-component on-close="callback">
        Com callback e botão de fechar
      </label-component>
    `)($rootScope);
    $rootScope.$digest();
    expect(element[0].querySelector('.label__close').textContent).toBe('x');
  });

});
