/**
 * @namespace @directive [ui] label-component
 * @description Usado como tag, etiqueta ou rótulo.
 * @property {string} [style] Pode escolher o estilo "brand"
 * @property {string} [color] Pode customizar a cor passando um valor hex
 * @property {function} [on-close] Callback para o botão de fechar do label
 * @example
 * <label-component style="success">Sucesso!</label-component>
 * <label-component color="#666">Com cor customizada</label-component>
 */

 import 'angular';

angular.module('labelComponent', [])
.directive('labelComponent', directive)
.run(($templateCache) => (
  $templateCache.put('components/ui/label-component', `
    <span class="label" ng-class="{
      'label--secondary': vm.style == 'brand'
    }" ng-style="vm.color ? {'background-color': vm.color} : null">
      <span class="label__child" ng-transclude></span>
      <span class="label__close" ng-if="vm.onClose" ng-click="vm.onClose()">x</span>
    </span>
  `)
));

function directive($templateCache) {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    scope: {
      style: '@',
      color: '@',
      size: '@',
      onClose: '='
    },
    controller: controller,
    controllerAs: 'vm',
    bindToController: true,
    template: $templateCache.get('components/ui/label-component')
  };
};

function controller($scope) {};
