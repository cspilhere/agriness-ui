import 'angular';
import scope from '../formfieldWrapper/scope';

angular
.module('formfieldSelect', [])
.directive('formfieldSelect', directive)
.run(template);

function template($templateCache) {
  $templateCache.put('components/ui/formfield-select', `
    <div class="form-component-wrapper">
      <formfield-wrapper>

      <select
        class="select"

        name="{{vm.name}}"

        ng-focus="null"
        ng-blur="null"
        ng-change="null"
        ng-model="model">

        <option value="" selected="true">
          {{vm.placeholder}}
        </option>

        <option value="item" ng-repeat="item in vm.options">
          {{item}}
        </option>

      </select>

      </formfield-wrapper>
    </div>
  `);
}

function directive() {
  return {
    restrict: 'E',
    replace: true,
    scope: scope,
    controller: controller,
    controllerAs: 'vm',
    bindToController: true,
    templateUrl: 'components/ui/formfield-select'
  };
};

function controller($scope) {
  // console.log($scope);
};
