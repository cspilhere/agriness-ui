/**
 * @namespace @directive [ui] alert
 * @description Mostra um alerta no campo superior direito.
 * @property {string} [style] Estilo do alerta: secondary, danger, success e attention
 * @property {function} [on-close] Callback para o botão de remover o alerta
 * @example
 * <alert
 *   style="success"
 *   is-active="alertIsOpen"
 *   on-close="onCloseAlert">
 *   Atenção! Sucesso. :)
 * </alert>
 */

import 'angular';

angular.module('alert', [])
.directive('alert', directive)
.run(($templateCache) => (
  $templateCache.put('components/ui/alert', `
    <span class="alert" ng-class="{
      'is-active': isActive,
      'alert--secondary': vm.style == 'brand',
      'alert--danger': vm.style == 'danger',
      'alert--attention': vm.style == 'attention',
      'alert--success': vm.style == 'success'
    }">
      <span class="alert__child" ng-transclude></span>
      <span class="alert__close" ng-if="vm.onClose" ng-click="onClose()">x</span>
    </span>
  `)
));

function directive($templateCache) {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    scope: {
      style: '@',
      color: '@',
      size: '@',
      onClose: '=',
      isActive: '='
    },
    controller: controller,
    controllerAs: 'vm',
    bindToController: true,
    template: $templateCache.get('components/ui/alert')
  };
};

function controller($scope) {
  $scope.isActive = false;

  $scope.$watch('vm.isActive', (newValue) => {
    $scope.isActive = newValue;
  });

  $scope.onClose = () => {
    $scope.isActive = false;
    if (typeof $scope.vm.onClose == 'function') $scope.vm.onClose();
  };
};
