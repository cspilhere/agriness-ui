import './';

describe('alert basic tests', () => {

  let $compile, $rootScope;

  beforeEach(angular.mock.module('alert'));

  beforeEach(angular.mock.inject((_$compile_, _$rootScope_) => {
    $compile = _$compile_;
    $rootScope = _$rootScope_;
  }));

  let element;

  beforeEach(() => {
    $rootScope.callback = () => {};
    element = $compile(`
      <alert
        style="success"
        is-active="true"
        on-close="callback">
        Atenção! Sucesso. :)
      </alert>
    `)($rootScope);
    $rootScope.$digest();
  });

  it('Adiciona as classes necessárias para o alerta abrir.', () => {
    expect(element[0].classList.contains('alert--success')).toBe(true);
    expect(element[0].classList.contains('is-active')).toBe(true);
  });

  it('Mostra o botão de fechar quando um callback for configurado.', () => {
    expect(element[0].querySelectorAll('.alert__close').length).toBe(1);
  });

});
