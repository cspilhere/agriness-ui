import './';

describe('buttonComponent basic tests', () => {

  let $compile, $rootScope;

  beforeEach(angular.mock.module('buttonComponent'));

  beforeEach(angular.mock.inject((_$compile_, _$rootScope_) => {
    $compile = _$compile_;
    $rootScope = _$rootScope_;
  }));

  it('O label do botão é configurado corretamente', () => {
    let element = $compile(`
      <button-component>Botão</button-component>
    `)($rootScope);
    $rootScope.$digest();
    expect(element.html()).toContain('Botão');
  });

  it('O tipo de botão passa a ser "submit"', () => {
    let element = $compile(`
      <button-component is-submit="true">Submeter</button-component>
    `)($rootScope);
    $rootScope.$digest();
    expect(element[0].getAttribute('type')).toBe('submit');
  });

});
