/**
 * @namespace @directive [ui] button-componnent
 * @description Pode ser configurado para apresentar diferentes estilos de botões.
 * @property {string} [style] Estilo do botão: vazado, atenção, com preenchimento, com ícone, etc
 * @property {string} [size] Altera o tamanho do botão
 * @property {boolean} [is-submit] Indica se o botão é do tipo "submit", indicado para submeter formulários
 * @example
 * <button-component style="outline">Cancelar</button-component>
 * <button-component is-submit="true">Enviar</button-component>
 */

 import 'angular';

angular.module('buttonComponent', [])
.directive('buttonComponent', directive)
.run(($templateCache) => (
  $templateCache.put('components/ui/button-component', `
    <button type="{{vm.isSubmit ? 'submit' : 'button'}}" class="button" ng-class="{
      'button--outline': vm.style == 'outline',
      'button--danger': vm.style == 'danger',
      'button--icon': vm.style == 'icon',
      'button--full': vm.size == 'full'
    }">
      <span class="button__label" ng-if="vm.style != 'icon'" ng-transclude></span>
      <span class="button__icon" ng-if="vm.style == 'icon'" ng-transclude></span>
    </button>
  `)
));

function directive($templateCache) {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    scope: {
      style: '@',
      isSubmit: '=?isSubmit',
      size: '@'
    },
    controller: controller,
    controllerAs: 'vm',
    bindToController: true,
    template: $templateCache.get('components/ui/button-component')
  };
};

function controller($scope) {};
