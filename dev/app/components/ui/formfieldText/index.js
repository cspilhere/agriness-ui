import 'angular';
import scope from '../formfieldWrapper/scope';

angular
.module('formfieldText', [])
.directive('formfieldText', directive)
.run(template);

function template($templateCache) {
  $templateCache.put('components/ui/formfield-text', `
    <div class="form-component-wrapper">

      <div class="form-component">

        <div class="form-component__label" ng-if="vm.title">
          {{vm.title}}
        </div>

        <label for="{{vm.name}}" class="form-component__field">

          <input
            class="input-text"
            type="{{vm.type === 'password' ? 'password' : 'text'}}"
            tabindex="0"

            name="{{vm.name}}"
            placeholder="{{vm.placeholder}}"
            ng-focus="null"
            ng-blur="null"
            ng-change="null"
            ng-model="model">

        </label>

      </div>

    </div>
  `);
}

function directive() {
  return {
    restrict: 'E',
    replace: true,
    scope: scope,
    link: link,
    controller: controller,
    controllerAs: 'vm',
    bindToController: true,
    require: '^?ngModel',
    templateUrl: 'components/ui/formfield-text'
  };
};

function link(scope, element, attrs, ngModel) {
  // ngModel.$setValidity('field.error', false);
  if (ngModel) {
    scope.ngModel = ngModel;
    ngModel.$render = () => element.val(ngModel.$modelValue);
  }
};

function controller($scope, $element, ParseValue) {

  let field = $element.find('input')[0];
  if (!field) return;

  $scope.parseValue = (value) => {
    let parser = ParseValue(value, this.mask, this.validator);
    this.isValid = parser.isValid;
    if (!parser.parsedValue || parser.parsedValue.length < 1) this.isValid = null;
    return parser;
  };

  field.addEventListener('input', (event) => {
    field.value = $scope.parseValue(field.value).parsedValue;
    $scope.ngModel.$setViewValue($scope.parseValue(field.value).parsedValue);
  });

  $scope.$watch('vm.value', (newValue, oldValue) => {
    $scope.model = newValue;
  });

};
