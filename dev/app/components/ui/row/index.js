/**
 * @namespace @directive [ui] row
 */

import 'angular';

angular.module('row', [])
.directive('row', directive)
.run(($templateCache) => (
  $templateCache.put('components/ui/row', `
    <div class="row" ng-class="{
      'row--spacing-small': vm.size == 'small',
      'row--spacing-medium': vm.size == 'medium',
      'row--spacing-large': vm.size == 'large'
    }"><ng-transclude></ng-transclude></div>
  `)
));

function directive($templateCache) {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    scope: {
      size: '@'
    },
    controller: controller,
    controllerAs: 'vm',
    bindToController: true,
    template: $templateCache.get('components/ui/row')
  };
};

function controller($scope) {};
