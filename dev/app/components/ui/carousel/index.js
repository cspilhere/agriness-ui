import Swiper from 'swiper';

/**
 * @namespace @directive [ui] carousel
 */

import 'angular';

angular.module('carousel', [])
.directive('carousel', directive)
.directive('carouselContent', carouselSlideDirective)
.run(($templateCache) => (
  $templateCache.put('components/ui/carousel', `
    <div class="swiper-main-container">
      <div class="swiper-container">
        <div class="swiper-wrapper" ng-transclude="slide"></div>
        <div class="swiper-button-prev" ng-if="vm.navigation != undefined"></div>
        <div class="swiper-button-next" ng-if="vm.navigation != undefined"></div>
        <div class="swiper-pagination"></div>
      </div>
    </div>
  `)
));

function directive($templateCache) {
  return {
    restrict: 'E',
    replace: true,
    transclude: {
      'slide': '?carouselContent',
    },
    scope: {
      navigation: '@'
    },
    controller: controller,
    controllerAs: 'vm',
    bindToController: true,
    template: $templateCache.get('components/ui/carousel')
  };
};

function controller($scope) {
  // console.log($scope);
};

function carouselSlideDirective() {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    controller: () => {
      new Swiper('.swiper-container', {
        pagination: {
          el: '.swiper-pagination',
          clickable: true
        },
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
        slidesPerView: 1,
        spaceBetween: 30,
        loop: true,
        autoHeight: true
      });
    },
    template: `<div class="swiper-slide" ng-transclude></div>`
  };
};

