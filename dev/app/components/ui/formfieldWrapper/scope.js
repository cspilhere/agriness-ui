export default {
  title: '@',
  name: '@',
  placeholder: '@',
  type: '@',
  mask: '@',
  value: '@',
  validator: '@',
  options: '=',
  eventDate: '@'
};

// fieldLabel: '@',
// fieldIsDisabled: '=?fieldIsDisabled',
// fieldIsOptional: '=?fieldIsOptional',

// fieldNoValidate: '@', // Flag

// fieldTip: '@',
// fieldDescription: '@',
// fieldMessage: '@',
// fieldStatus: '@',

// lockValue: '=?lockValue',
// onLockValue: '=?onLockValue',

// fieldPlaceholder: '@',
// fieldMask: '@',
// fieldValidator: '@',

// fieldIsValidating: '=?fieldIsValidating',

// fieldValue: '=?fieldValue',

// fieldOnFocus: '=',

// fieldType: '@?fieldType',
// fieldStyle: '@'
