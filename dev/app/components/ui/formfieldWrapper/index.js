import 'angular';

angular
.module('formfieldWrapper', [])
.directive('formfieldWrapper', directive)
.run(($templateCache) => (
  $templateCache.put('components/ui/formfield-wrapper', `
    <div class="form-component">

      <div class="form-component__label" ng-if="vm.title">
        {{vm.title}}
      </div>

      <label for="{{vm.name}}" class="form-component__field">

        <ng-transclude></ng-transclude>

      </label>

    </div>
  `)
));

function directive($templateCache) {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    scope: true,
    controller: controller,
    controllerAs: 'vm',
    link: link,
    bindToController: true,
    // require: 'ngModel',
    template: $templateCache.get('components/ui/formfield-wrapper')
  };
};

function link(scope, element, attrs, ngModel) {

  // ngModel.$setViewValue(newValue);
  // ngModel.$setValidity('field.error', false);
  // ngModel.$render = () => element.val(ngModel.$modelValue);

};

function controller($scope) {
  $scope.vm = $scope.$parent.vm;
  console.log($scope.vm);
};
