import './';

describe('icon basic tests', () => {

  let $compile, $rootScope;

  beforeEach(angular.mock.module('icon'));

  beforeEach(angular.mock.inject((_$compile_, _$rootScope_) => {
    $compile = _$compile_;
    $rootScope = _$rootScope_;
  }));

  it('A classe do ícone é adicionada corretamente', () => {
    let element = $compile('<icon name="hand"></icon>')($rootScope);
    $rootScope.$digest();
    expect(element[0].classList.contains('fa-hand')).toBe(true);
  });

});
