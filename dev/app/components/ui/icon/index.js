/**
 * @namespace @directive [ui] icon
 * @description Componente que renderiza um ícone.
 * @property {string} name Nome do ícone que deverá ser encontrado na biblioteca de ícones
 * @property {string} [size] Configurar o tamanho do ícone
 * @example
 * <icon name="hand"></icon>
 * <icon name="hand" size="large"></icon>
 */

 import 'angular';

angular.module('icon', [])
.directive('icon', directive)
.run(($templateCache) => (
  $templateCache.put('components/ui/icon', `
    <i class="fa fa-{{vm.name}}" ng-style="vm.color ? {'color': vm.color} : null" aria-hidden="true" style="cursor: pointer;"></i>
  `)
));

function directive($templateCache) {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    scope: {
      name: '@',
      size: '@',
      color: '@'
    },
    controller: controller,
    controllerAs: 'vm',
    bindToController: true,
    template: $templateCache.get('components/ui/icon')
  };
};

function controller($scope) {};
