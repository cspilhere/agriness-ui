import 'angular';
import scope from '../formfieldWrapper/scope';

// https://github.com/assisrafael/angular-input-masks

angular
.module('switchFieldWeight', [])
.directive('switchFieldWeight', directive)
.run(template);

function template($templateCache) {
  $templateCache.put('components/ui/switch-field-weight', `
    <div class="form-component-wrapper">

      <div class="field-primary" ng-show="showPrimary">
        <formfield-text
          title="Peso Médio"
          name="averageWeight"
          mask="decimals(2)"
          validator=""
          ng-model="model1"
          placeholder="Placeholder do campo">
        </formfield-text>
      </div>

      <div class="field-secondary" ng-show="!showPrimary">
        <formfield-text
          title="Peso Total"
          name="totalWeight"
          mask="decimals(2)"
          validator=""
          ng-model="model2"
          placeholder="Placeholder do campo">
        </formfield-text>
      </div>

      Usar {{!showPrimary ? 'peso médio' : 'peso total'}}? <span ng-click="toggleSecundary()">{{showPrimary ? 'sim' : 'não'}}</span>

    </div>
  `);
}

function directive() {
  return {
    restrict: 'E',
    scope: scope,
    link: link,
    controller: controller,
    controllerAs: 'vm',
    bindToController: true,
    require: '^?ngModel',
    templateUrl: 'components/ui/switch-field-weight'
  };
};

function link(scope, element, attrs, ngModel) {

  if (ngModel) {
    scope.ngModel = ngModel;
    ngModel.$render = () => element.val(ngModel.$modelValue);
  }

};

function controller($scope, $timeout) {

  $scope.showPrimary = true;

  $scope.toggleSecundary = () => {
    $scope.showPrimary = !$scope.showPrimary;
    $scope.ngModel.$setViewValue($scope.showPrimary ? $scope.model1 : $scope.model2);
  };

  $scope.$watch('model1', () => {
    $scope.ngModel.$setViewValue($scope.model1);
  });

  $scope.$watch('model2', () => {
    $scope.ngModel.$setViewValue($scope.model2);
  });

};
