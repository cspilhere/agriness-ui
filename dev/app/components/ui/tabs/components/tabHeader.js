import 'angular';

angular
.module('tabHeader', [])
.directive('tabHeader', directive);

function directive() {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    scope: {
      tabHeaderId: '@'
    },
    controller: ($rootScope, $scope) => {

      if (!$scope.$parent.$parent.vm) $scope.$parent.$parent.vm = $scope.$parent.$parent.$parent.vm;

      $scope.isActive = $scope.tabHeaderId === $scope.$parent.$parent.vm.activeTab;

      // $scope.$parent.$parent.vm.allTabs.push($scope.tabHeaderId);

      $scope.$watch('$parent.$parent.vm.activeTab', (newValue, oldValue) => {
        if (!newValue) return;
        if (newValue === oldValue) return;
        $scope.isActive = $scope.tabHeaderId === newValue;
      });

      $scope.changeTab = () => {
        if ($scope.$parent.$parent.vm.tabsName) {
          $rootScope.tabs[$scope.$parent.$parent.vm.tabsName] = $scope.tabHeaderId;
        }
        $scope.$parent.$parent.vm.activeTab = $scope.tabHeaderId;
        // store.set('activeTab', $scope.tabHeaderId);
        // let params = {
        //   'stage': $rootScope.farmStageName,
        //   'tab': $scope.tabHeaderId,
        //   'gender': $rootScope.$stateParams.gender
        // };
        // $state.go($state.current.name, params);
      };
    },
    template: `
      <li class="tabs__nav-item" ng-class="{ 'is-active': isActive }" ng-click="changeTab()">
        <a><ng-transclude></ng-transclude></a>
      </li>
    `
  };
};
