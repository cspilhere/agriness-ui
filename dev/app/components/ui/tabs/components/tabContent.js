import 'angular';

angular
.module('tabContent', [])
.directive('tabContent', directive);

function directive() {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    scope: {
      tabContentHeader: '@'
    },
    controller: ($scope) => {
      if (!$scope.$parent.$parent.vm) $scope.$parent.$parent.vm = $scope.$parent.$parent.$parent.vm;
      $scope.isActive = $scope.tabContentHeader === $scope.$parent.$parent.vm.activeTab;

      $scope.$parent.$parent.vm.allTabs.push($scope.tabContentHeader);

      $scope.$watch('$parent.$parent.vm.activeTab', (newValue, oldValue) => {
        if (!newValue) return;
        if (newValue === oldValue) return;
        $scope.isActive = $scope.tabContentHeader === newValue;
      });
    },
    template: `
      <div class="tabs__content" ng-class="{ 'is-active': isActive }">
        <div class="container" ng-if="isActive"><ng-transclude></ng-transclude></div>
      </div>
    `
  };
};
