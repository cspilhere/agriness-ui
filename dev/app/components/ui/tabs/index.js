/**
 * @namespace @directive [ui] tabs
 * @description Abas para organizar e/ou agrupar conteúdo.
 * @property {string} tabs-initial Chave da aba que deve iniciar aberta
 * @property {string} tabs-name Chave identificadora das abas no $rootScope
 * @property {string} tab-header-id (tab-header) Chave da aba
 * @property {string} tab-content-header (tab-content) Chave da aba
 * @example
 * <tabs tabs-initial="tab2" tabs-name="tabs">
 *  <tab-header tab-header-id="tab1">Aba 1</tab-header>
 *  <tab-header tab-header-id="tab2">Aba 2</tab-header>
 *  <tab-content tab-content-header="tab1">
 *    Tab 1 Content
 *  </tab-content>
 *  <tab-content tab-content-header="tab2">
 *    Tab 2 Content
 *  </tab-content>
 * </tabs>
 */

import 'angular';

import './components/tabHeader'
import './components/tabContent'

angular
.module('tabs', ['tabHeader', 'tabContent'])
.directive('tabs', directive)
.run(template)
.run(($rootScope) => $rootScope.tabs = $rootScope.tabs || {}) // Set tabs object on $rootScope

function template($templateCache) {
  $templateCache.put('directives/containers/tabs.template.html', `
    <div class="tabs">
      <ul class="tabs__nav" role="tablist"><div ng-transclude="header"></div></ul>
      <div class="tabs__content-wrapper"><div ng-transclude="content"></div></div>
    </div>
  `);
};

function directive() {
  return {
    restrict: 'E',
    replace: true,
    transclude: {
      header: '?tabHeader',
      content: '?tabContent'
    },
    scope: {
      tabsInitial: '@',
      tabsName: '@'
    },
    controller: controller,
    controllerAs: 'vm',
    bindToController: true,
    templateUrl: 'directives/containers/tabs.template.html'
  };
};

function controller($rootScope, $scope, $attrs) {

  this.tabsInitial = $attrs.tabsInitial;
  this.tabsName = $attrs.tabsName;
  this.allTabs = [];
  let tabsArray = [];

  // if (!$state.params[this.tabsName]) {
    this.activeTab = this.tabsInitial || null;
  // } else {
    // this.activeTab = $state.params[this.tabsName] || null;
  // }

  if (this.tabsName) {
    $rootScope.tabs[this.tabsName] = this.activeTab;
  }

  $scope.$on('$destroy', () => {
    $rootScope.tabs[this.tabsName] = null;
  });

  $scope.$watchCollection('vm.allTabs', (newValue) => {
    if (this.allTabs.length < 1) return;
    // if ($state.params.tab) {
    //   if (newValue.indexOf($state.params.tab) == -1) {
    //     let params = {
    //       'stage': $rootScope.farmStageName,
    //       'tab': this.tabsInitial,
    //       'gender': $rootScope.$stateParams.gender
    //     };
    //     $state.go($state.current.name, params);
    //   }
    // }
  });

};
