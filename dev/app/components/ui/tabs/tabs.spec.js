import './';

describe('tabs basic tests', () => {

  let $compile, $rootScope;

  beforeEach(angular.mock.module('tabs'));

  beforeEach(angular.mock.inject((_$compile_, _$rootScope_) => {
    $compile = _$compile_;
    $rootScope = _$rootScope_;
  }));

  let element;

  beforeEach(() => {
    element = $compile(`
      <tabs tabs-initial="tab2" tabs-name="tabs">

        <tab-header tab-header-id="tab1">Aba 1</tab-header>
        <tab-header tab-header-id="tab2">Aba 2</tab-header>
        <tab-header tab-header-id="tab3">Aba 3</tab-header>

        <tab-content tab-content-header="tab1">Conteúdo 1</tab-content>
        <tab-content tab-content-header="tab2">Conteúdo 2</tab-content>
        <tab-content tab-content-header="tab3">Conteúdo 3</tab-content>

      </tabs>
    `)($rootScope);
    $rootScope.$digest();
  });

  it('Inicia com a tab2 aberta.', () => {
    expect($rootScope.tabs.tabs).toBe('tab2');
    expect(element[0].querySelectorAll('.tabs__nav-item')[1].classList.contains('is-active')).toBe(true);
  });

  it('Muda para a tab1.', () => {
    angular.element(element[0].querySelectorAll('.tabs__nav-item')[0]).triggerHandler('click');
    expect(element[0].querySelectorAll('.tabs__nav-item')[1].classList.contains('is-active')).toBe(false);
    expect(element[0].querySelectorAll('.tabs__nav-item')[0].classList.contains('is-active')).toBe(true);
  });

  it('Checa se o $rootScope identifica a aba atual.', () => {
    angular.element(element[0].querySelectorAll('.tabs__nav-item')[0]).triggerHandler('click');
    expect($rootScope.tabs.tabs).toBe('tab1');
  });

});
