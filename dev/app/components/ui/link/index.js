/**
 * @namespace @directive [ui] link
 */

import 'angular';

angular.module('link', [])
.directive('link', directive)
.run(($templateCache) => (
  $templateCache.put('components/ui/link', `
    <a class="link" href="{{vm.href}}">
      <span ng-transclude></span>
    </a>
  `)
));

function directive($templateCache) {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    scope: {
      href: '@'
    },
    controller: controller,
    controllerAs: 'vm',
    bindToController: true,
    template: $templateCache.get('components/ui/link')
  };
};

function controller($scope) {};
