import './';

describe('card basic tests', () => {

  let $compile, $rootScope;

  beforeEach(angular.mock.module('card'));

  beforeEach(angular.mock.inject((_$compile_, _$rootScope_) => {
    $compile = _$compile_;
    $rootScope = _$rootScope_;
  }));

  let element;

  it('O card renderiza todos as suas partes.', () => {
    $rootScope.callback = () => {};
    element = $compile(`
      <card
        title="Ahoy Card!"
        description="Descrição do card"
        callback-data="{ foo: 'bar' }"
        on-click-secondary="callback"
        secondary-button-label="Detalhes"
        on-click-primary="callback"
        primary-button-label="Entrar">
      </card>
    `)($rootScope);
    $rootScope.$digest();
    expect(element[0].querySelector('.card-central__header .heading').textContent).toBe('Ahoy Card!');
    expect(element[0].querySelector('.card-central__body .text').textContent).toBe('Descrição do card');
    expect(element[0].querySelectorAll('.card-central__footer button').length).toBe(2);
  });

  it('O card renderiza sem os botões.', () => {
    $rootScope.callback = () => {};
    element = $compile(`
      <card
        title="Ahoy Card!"
        description="Descrição do card">
      </card>
    `)($rootScope);
    $rootScope.$digest();
    expect(element[0].querySelectorAll('.card-central__footer button').length).toBe(0);
  });

});
