/**
 * @namespace @directive [ui] card
 * @description Card de informações composto por cabeçalho, corpo e rodapé.
 * @property {string} title Título do card
 * @property {string} description Descrição do card
 * @property {expression} callback-data Pode ser passado qualquer tipo de dado que será resgatado como parâmetro do callback
 * @property {function} on-click-primary Callback do botão primário
 * @property {string} primary-button-label Label do botão primário
 * @property {function} on-click-secondary Callback do botão secundário
 * @property {string} secondary-button-label Label do botão secundário
 * @example
 * <card
 *  title="Relatório"
 *  description="Um relatório fantástico cheio de gráficos incríveis."
 *  callback-data="{}"
 *  on-click-primary="callback"
 *  primary-button-label="Entrar">
 *  on-click-secondary="callback"
 *  secondary-button-label="Detalhes"
 *  <label-component color="#666">Top</label-component>
 *  <label-component>Topzeira</label-component>
 * </card>
 */

import 'angular';

angular
.module('card', [])
.directive('card', directive)
.run(($templateCache) => (
  $templateCache.put('components/ui/card', `
    <div class="card-central">

      <header class="card-central__header" ng-style="{
        'text-align': vm.align
      }">
        <h3 class="heading heading--regular">{{vm.title}}</h3>
      </header>

      <div class="card-central__body">
        <p class="text text--small">{{vm.description}}</p>

        <ng-transclude></ng-transclude>

      </div>

      <footer class="card-central__footer" ng-class="{
        'card-central__footer--align-left': vm.align == 'left',
        'card-central__footer--align-center': vm.align == 'center'
      }">
        <span></span>
        <span>
          <button
            ng-click="vm.onClickSecondary(vm.callbackData)"
            class="button button--small button--outline"
            ng-if="vm.secondaryButtonLabel">
            <span class="button__label">{{vm.secondaryButtonLabel}}</span>
          </button>
          <button
            ng-click="vm.onClickPrimary(vm.callbackData)"
            class="button button--small"
            ng-if="vm.primaryButtonLabel">
            <span class="button__label">{{vm.primaryButtonLabel}}</span>
          </button>
        </span>
      </footer>

    </div>
  `)
));

function directive($templateCache) {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    scope: {
      title: '@',
      description: '@',
      callbackData: '=',
      onClickPrimary: '=',
      primaryButtonLabel: '@',
      onClickSecondary: '=',
      secondaryButtonLabel: '@',
      align: '@'
    },
    controller: controller,
    controllerAs: 'vm',
    bindToController: true,
    template: $templateCache.get('components/ui/card')
  };
};

function controller($scope) {};
