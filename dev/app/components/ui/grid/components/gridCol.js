/**
 * @namespace @directive [ui] tabs
 * @description Abas para organizar e/ou agrupar conteúdo.
 * @property {string} tabs-initial Chave da aba que deve iniciar aberta
 * @property {string} tabs-name Chave identificadora das abas no $rootScope
 * @property {string} tab-header-id (tab-header) Chave da aba
 * @property {string} tab-content-header (tab-content) Chave da aba
 * @example
 * <tabs tabs-initial="tab2" tabs-name="tabs">
 *  <tab-header tab-header-id="tab1">Aba 1</tab-header>
 *  <tab-header tab-header-id="tab2">Aba 2</tab-header>
 *  <tab-content tab-content-header="tab1">
 *    Tab 1 Content
 *  </tab-content>
 *  <tab-content tab-content-header="tab2">
 *    Tab 2 Content
 *  </tab-content>
 * </tabs>
 */

import 'angular';

angular
.module('gridCol', [])
.directive('gridCol', directive)
.run(template);

function template($templateCache) {
  $templateCache.put('components/ui/grid-col', `
    <div class="grid__col{{vm.size ? ' grid__col--lg-' + vm.size : ''}}">
      <ng-transclude></ng-transclude>
    </div>
  `);
};

function directive() {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    scope: {
      size: '@',
      align: '@'
    },
    controller: controller,
    controllerAs: 'vm',
    bindToController: true,
    templateUrl: 'components/ui/grid-col'
  };
};

function controller() {};
