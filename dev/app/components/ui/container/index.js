/**
 * @namespace @directive [ui] container
 * @description Container genérico de conteúdo
 * @property {string} title Título do container
 * @example
 * <container title="Container">
 *  Com título
 * </container>
 *
 * <container>
 *  Sem título
 * </container>
 */

import 'angular';

angular
.module('container', [])
.directive('container', directive)
.run(($templateCache) => (
  $templateCache.put('components/ui/container', `
    <div class="container container--primary">
      <header class="main-container__header" ng-if="vm.title">
        <h1 class="heading heading--regular">{{vm.title}}</h1>
      </header>
      <div class="main-container__body" ng-transclude></div>
    </div>
  `)
));

function directive($templateCache) {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    scope: {
      title: '@'
    },
    controller: controller,
    controllerAs: 'vm',
    bindToController: true,
    template: $templateCache.get('components/ui/container')
  };
};

function controller($scope) {};
