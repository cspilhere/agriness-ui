import './';

describe('container basic tests', () => {

  let $compile, $rootScope;

  beforeEach(angular.mock.module('container'));

  beforeEach(angular.mock.inject((_$compile_, _$rootScope_) => {
    $compile = _$compile_;
    $rootScope = _$rootScope_;
  }));

  let element;

  it('O container é renderizado com o título.', () => {
    $rootScope.callback = () => {};
    element = $compile(`
      <container title="Container">
        Com título
      </container>
    `)($rootScope);
    $rootScope.$digest();
    expect(element[0].querySelector('.main-container__header .heading').textContent).toBe('Container');
  });

  it('O container é renderizado sem título.', () => {
    $rootScope.callback = () => {};
    element = $compile(`
      <container>
        Sem título
      </container>
    `)($rootScope);
    $rootScope.$digest();
    expect(element[0].querySelector('.main-container__header')).toBe(null);
  });

});
