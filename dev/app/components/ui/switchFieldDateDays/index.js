import 'angular';
import scope from '../formfieldWrapper/scope';
import moment from 'moment';

// https://github.com/assisrafael/angular-input-masks

angular
.module('switchFieldDateDays', [])
.directive('switchFieldDateDays', directive)
.run(template);

function template($templateCache) {
  $templateCache.put('components/ui/switch-field-date-days', `
    <div class="form-component-wrapper">

      <div class="field-primary" ng-show="isDate">
        <formfield-text
          title="Data"
          name="Data"
          mask="date"
          validator=""
          ng-model="model1"
          value="{{birthdate}}"
          placeholder="Placeholder do campo">
        </formfield-text>
      </div>

      <div class="field-secondary" ng-show="!isDate">
        <formfield-text
          title="Dias"
          name="days"
          mask=""
          validator=""
          ng-model="model2"
          value="{{age}}"
          placeholder="Placeholder do campo">
        </formfield-text>
      </div>

      {{!isDate ? 'Data de nascimento: ' + birthdate || 'não informado' : 'Idade: ' + age}} <span ng-click="toggleSecundary()">{{!isDate ? '(usar data)' : '(usar dias)'}}</span>

      </div>
    `);
  }

function directive() {
  return {
    restrict: 'E',
    scope: scope,
    link: link,
    controller: controller,
    controllerAs: 'vm',
    bindToController: true,
    require: '^?ngModel',
    templateUrl: 'components/ui/switch-field-date-days'
  };
};

function link(scope, element, attrs, ngModel) {

  if (ngModel) {
    scope.ngModel = ngModel;
    ngModel.$render = () => element.val(ngModel.$modelValue);
  }

};

function controller($scope, $timeout) {

  $scope.isDate = true;

  $scope.birthdate = '';
  $scope.age = '';

  const eventDate = dateLocale($scope.vm.eventDate);

  $scope.toggleSecundary = () => {
    $scope.isDate = !$scope.isDate;
    $scope.ngModel.$setViewValue($scope.isDate ? $scope.model1 || $scope.birthdate : $scope.model2 || $scope.age);

    $scope.age = moment(eventDate).diff(dateLocale($scope.model1), 'days');
    $scope.birthdate = moment(eventDate).subtract(dateLocale($scope.model2), 'days').format('DD/MM/YYYY');
  };

  $scope.$watch('model1', () => {
    $scope.ngModel.$setViewValue($scope.model1);
    $scope.age = moment(eventDate).diff(dateLocale($scope.model1), 'days');
  });

  $scope.$watch('model2', () => {
    $scope.ngModel.$setViewValue($scope.model2);
    $scope.birthdate = moment(eventDate).subtract(dateLocale($scope.model2), 'days').format('DD/MM/YYYY');
  });

  $scope.$watch('vm.eventDate', (newValue, oldValue) => {

  });

};


function dateLocale(date, format) {
  if(!date) return;
  if(!format) format = 'yyyy-MM-dd HH:mm';
  const datetimeFormat = format || User.me().formats['Data e hora'].format;
  const dateHasHour = !!date.split(" ")[1];
  const datetimeArray = datetimeFormat.split(" ");
  const dateFormat = datetimeArray[0].toUpperCase() + (dateHasHour ? " " + datetimeArray[1] : '');
  if(date.includes('/') && dateFormat.includes('-')) { //pt-br to en-us
    const originalFormat = 'DD/MM/YYYY' + (dateHasHour ? " HH:mm" : '');
    return moment(date, originalFormat).format(dateFormat);
  } else if(date.includes('-') && dateFormat.includes('/')) { //en-us to pt-br
    const originalFormat = 'YYYY-MM-DD' + (dateHasHour ? " HH:mm" : '');
    return moment(date, originalFormat).format(dateFormat);
  }
  return date;
};
