function template($templateCache) {

  $templateCache.put('directives/datatable/datatable.template.html', `
    <div class="table-container">

      <div class="table__top">

        <div class="table-status" style="margin-bottom: 4px;">
          <span style="text-align: left;">{{vm.tableStatus != null ? vm.tableStatus : '&nbsp;'}}</span>
        </div>

        <div id="tableControl" class="button-group button-group--spacing" ng-if="(!vm.hideControls && vm.model.length > 0)">
          <button type="button" class="button button--icon" ng-click="vm.exportPDF()" title="Exportar como PDF">
            <span class="button__icon">
              <svg xmlns="http://www.w3.org/2000/svg" class="icon">
                <use xlink:href="./media/sprites/sprite.defs.svg#exportar_paginas-pdf"></use>
              </svg>
            </span>
          </button>
          <button
            type="button"
            class="button button--icon"
            ng-click="vm.exportCSV()"
            style="margin-left: 0;"
            title="Exportar como CSV">
            <span class="button__icon">
              <svg xmlns="http://www.w3.org/2000/svg" class="icon">
                <use xlink:href="./media/sprites/sprite.defs.svg#exportar_paginas"></use>
              </svg>
            </span>
          </button>
        </div>

        <div
          id="tableColumnVisibility"
          class="dropdown dropdown--light is-last"
          data-toggle-id="tableControls"
          ng-if="(!vm.hideColumnsVisibility && vm.model.length > 0)">
          <span class="dropdown__header" toggle toggle-target="tableControls">
            <button type="button" class="button button--icon" title="Colunas">
              <span class="button__icon">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon">
                  <use xlink:href="./media/sprites/sprite.defs.svg#configurar_colunas2"></use>
                </svg>
              </span>
            </button>
          </span>
          <div class="dropdown__content">
            <ul class="list">
              <li class="list__item" ng-repeat="column in vm.tableHeaderMap track by $index">
                <label class="checkbox">
                  <input
                    type="checkbox"
                    name="checkbox"
                    class="checkbox__input"
                    ng-init="col[column.id] = column.isActive"
                    ng-model="col[column.id]"
                    ng-change="vm.updateColumns(column.name)">
                  <span class="checkbox__token"></span>
                  <span class="checkbox__label">{{ column.name }}</span>
                </label>
              </li>
            </ul>
          </div>
        </div>
      </div>

      <div class="table-wrapper">

        <table class="table">

          <thead class="table__header">
            <tr class="table__row">

              <th
                class="table__col"
                scope="col"
                ng-show="tableHeader.isActive"
                ng-click="vm.sort(tableHeader.path, tableHeader.sort)"
                ng-class="{
                  'table__col--align-right': tableHeader.align == 'right',
                  'table__col--align-center': tableHeader.align == 'center'
                }"
                ng-repeat="tableHeader in vm.tableHeaderMap track by $index">

                <span>{{tableHeader.name}}</span>

                <span
                  ng-show="vm.sortKey === tableHeader.path"
                  ng-class="{ 'more-up': vm.reverse, 'more-down': !vm.reverse }">
                </span>

              </th>

              <th
                class="table__col"
                scope="col"
                colspan="100"
                ng-if="vm.showOptions"
                style="width: 1px;">
              </th>

            </tr>
          </thead>

          <tbody class="table__body" ng-transclude></tbody>

        </table>

      </div>

    </div>
  `);

  $templateCache.put('directives/datatable/datatable-row.template.html', `
    <tr
      class="table__row"
      ng-repeat="tableRow in tableRows | orderBy:sortKey:reverse | limitTo:limitTo track by $index"
      ng-class="{
        'highlight-first-row': tableRow.highlightFirstRow,
        'highlight-last-row': tableRow.highlightLastRow
      }"
      repeater-info
      ng-transclude>
    </tr>
  `);

  $templateCache.put('directives/datatable/datatable-column.template.html', `
    <td
      class="table__col"
      ng-class="{
        'table__col--options': showOptions,
        'is-clickable': isClickable,
        'table__col--small-width': width == 'small',
        'table__col--align-right': align == 'right',
        'table__col--align-center': align == 'center'
      }"
      data-options-column="{{showOptions}}"
      ng-show="isActive"
      ng-click="rowOnclickHandler($event)">
      <ng-transclude></ng-transclude>
    </td>
  `);

};

export default template;
