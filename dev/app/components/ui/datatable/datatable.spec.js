import './';

const mock = [{
  name: 'Cordelia Klein',
  email: 'katu@hucohe.dj',
  status: { name: 'Ativo' }
}, {
  name: 'Micheal Huff',
  email: 'cofebo@pi.ne',
  status: { name: 'Inativo' }
}, {
  name: 'May Nguyen',
  email: 'vankami@wisazulo.er',
  status: { name: 'Ativo' }
}, {
  name: 'Nancy King',
  email: 'ju@ulak.mx',
  status: { name: 'Inativo' }
}];

describe('datatable basic tests', () => {

  let $compile, $rootScope;

  beforeEach(angular.mock.module('datatable'));

  beforeEach(angular.mock.inject((_$compile_, _$rootScope_) => {
    $compile = _$compile_;
    $rootScope = _$rootScope_;
  }));

  let element;

  beforeEach(() => {
    $rootScope.mock = mock;
    $rootScope.options = true;
    $rootScope.hideColumnsVisibility = false;
    $rootScope.hideControls = false;
    element = $compile(`
      <datatable
        model="mock"
        show-options="options"
        hide-columns-visibility="hideColumnsVisibility"
        hide-controls="hideControls">

        <datatable-row>

          <datatable-column column-name="Nome" column-path="name">
            <col-wrapper>{{$row.name}}</col-wrapper>
          </datatable-column>

          <datatable-column column-name="Email" column-path="email">
            <col-wrapper>{{$row.email}}</col-wrapper>
          </datatable-column>

          <datatable-column ng-if="options">
            Opções
          </datatable-column>

        </datatable-row>

      </datatable>
    `)($rootScope);
    $rootScope.$digest();
  });

  it('A tabela renderiza o número de linhas corretamente.', () => {
    expect(element[0].querySelectorAll('tbody tr').length).toBe(mock.length);
  });

  it('Esconde a coluna de opções.', () => {
    $rootScope.options = false;
    $rootScope.$digest();
    expect(element[0].querySelectorAll('thead th').length).toBe(2);
    expect(element[0].querySelectorAll('tbody tr:first-child td').length).toBe(2);
  });

  it('Esconde os controles da tabela.', () => {
    $rootScope.options = true;
    $rootScope.hideControls = true;
    $rootScope.$digest();
    expect(element[0].querySelector('#tableControll')).toBeNull();
  });

  it('Esconde as opções de coluna da tabela.', () => {
    $rootScope.options = true;
    $rootScope.hideColumnsVisibility = true;
    $rootScope.$digest();
    expect(element[0].querySelector('#tableColumnVisibility')).toBeNull();
  });

  it('Esconde as opções de coluna e os controles da tabela quando o model estiver vazio.', () => {
    $rootScope.options = true;
    $rootScope.hideControls = false;
    $rootScope.hideColumnsVisibility = false;
    $rootScope.mock = [];
    $rootScope.$digest();
    expect(element[0].querySelector('#tableControll')).toBeNull();
    expect(element[0].querySelector('#tableColumnVisibility')).toBeNull();
  });

});
