import jsPDF from 'jspdf';
import 'jspdf-autotable';


function toJSONLocal (date) {
  var local = new Date(date);
  local.setMinutes(date.getMinutes() - date.getTimezoneOffset());
  return local.toJSON().slice(0, 10);
}

const exportToPDF = (fileName, headerMap) => {

  let trs = document.querySelectorAll('tr');
  let array = [];
  let keys = Object.keys(headerMap);

  trs.forEach((item) => {
    let trObject = [];
    item.querySelectorAll('td').forEach((td, index) => {
      if (td.getAttribute('data-options-column') != 'true') {
        if (td.querySelector('[data-to-export-only="true"]')) {
          if (headerMap[keys[index]] && headerMap[keys[index]].isActive) {
            if (!trObject[keys[index]]) {
              trObject.push(td.querySelector('[data-to-export-only="true"]').textContent.trim());
            }
          }
        } else {
          if (headerMap[keys[index]] && headerMap[keys[index]].isActive) {
            if (!trObject[keys[index]]) trObject.push(td.textContent.trim());
          }
        }
      }
    });
    if (Object.keys(trObject).length > 0) array.push(trObject);
  });

  var doc = new jsPDF('l', 'pt');

  keys = keys.filter((item) => headerMap[item].isActive);

  doc.autoTable(keys, array, {
    tableLineColor: [229, 229, 229],
    tableLineWidth: 0.75,
    styles: {
      lineColor: [229, 229, 229],
      lineWidth: 0.75
    },
    headerStyles: {
      fillColor: [53, 57, 57],
      fontSize: 10
    },
    bodyStyles: {
      fillColor: [255, 255, 255],
      textColor: 0
    },
    alternateRowStyles: {
      fillColor: [255, 255, 255]
    },
    margin: { top: 60 },
    addPageContent: function(data) {
      doc.text('PDF ' + toJSONLocal(new Date()), 40, 50);
      var str = data.pageCount;
      if (typeof doc.putTotalPages === 'function') str = str + '';
      doc.setFontSize(10);
      doc.text(str, data.settings.margin.left, doc.internal.pageSize.height - 22);
    }
  });
  doc.save(fileName + '.pdf');
};

export default exportToPDF;
