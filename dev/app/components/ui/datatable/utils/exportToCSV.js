// https://stackoverflow.com/questions/41028114/how-to-convert-array-of-nested-objects-to-csv
function pivot(arr) {
  var mp = new Map();
  function setValue(a, path, val) {
    if (Object(val) !== val) { // primitive value
      var pathStr = path.join('.');
      var i = (mp.has(pathStr) ? mp : mp.set(pathStr, mp.size)).get(pathStr);
      a[i] = val;
    } else {
      for (var key in val) {
        setValue(a, key == '0' ? path : path.concat(key), val[key]);
      }
    }
    return a;
  }
  var result = arr.map(obj => setValue([], [], obj));
  return [[...mp.keys()], ...result];
}

function toCsv(arr) {
  return arr.map(row => row.map(val => isNaN(val) ? JSON.stringify(val) : +val).join(',')).join('\n');
}
  
const exportToCSV = (fileName, headerMap) => {
  let trs = document.querySelectorAll('tr');
  let array = [];
  let keys = Object.keys(headerMap);
  trs.forEach((item) => {
    let trObject = {};
    item.querySelectorAll('td').forEach((td, index) => {
      if (td.getAttribute('data-options-column') != 'true') {
        if (td.querySelector('[data-to-export-only="true"]')) {
          if (headerMap[keys[index]] && headerMap[keys[index]].isActive) {
            if (!trObject[keys[index]]) {
              trObject[keys[index]] = td.querySelector('[data-to-export-only="true"]').textContent.trim();
            }
          }
        } else {
          if (headerMap[keys[index]] && headerMap[keys[index]].isActive) {
            if (!trObject[keys[index]]) trObject[keys[index]] = td.textContent.trim();
          }
        }
      }
    });
    if (Object.keys(trObject).length > 0) array.push(trObject);
  });

  let a = document.createElement('a');
  a.style.display = 'none';
  a.download = fileName + '.csv';
  document.body.appendChild(a);
  a.href = 'data:text/csv;charset=UTF-8,' + encodeURIComponent(toCsv(pivot(array)));
  a.click();
  a.remove();  
};

export default exportToCSV;
