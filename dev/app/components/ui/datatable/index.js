import 'angular';

import templates from './template';
// import { arrayClearDuplicates } from '../../../utils/utils';
import exportToCSV from './utils/exportToCSV';
import exportToPDF from './utils/exportToPDF';

angular
.module('datatable', [])
.directive('datatable', directive)
.run(templates)
.directive('datatableRow', datatableRowDirective)
.directive('datatableColumn', datatableColumnDirective)
.directive('colWrapper', colWrapperDirective)
.directive('toExportOnly', toExportOnlyDirective);

function directive() {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    scope: {
      model: '=',
      defaultSorting: '@',
      maxRows: '@',
      columnsVisibility: '=?columnsVisibility',
      onChangeColumns: '=?onChangeColumns',

      showOptions: '=?showOptions',
      hideControls: '=?hideControls',
      hideColumnsVisibility: '=?hideColumnsVisibility'
    },
    controller: controller,
    controllerAs: 'vm',
    bindToController: true,
    templateUrl: () => 'directives/datatable/datatable.template.html'
  };
};

function controller($scope, $timeout, $element) {

  let headerMap = [];

  this.tableHeader = {};
  this.tableHeaderMap = {};
  this.showOptions = !!this.showOptions;

  this.limitTo = this.maxRows;

  this.tableStatus = 'Carregando...';

  this.sortKey = this.defaultSorting;

  this.sort = (key, isSortable) => {
    if (!isSortable) return;
    if (!key) return;
    this.sortKey = key;
    this.reverse = !this.reverse;
  };

  this.sort(this.sortKey);


  /**
   * Watchers and listeners
   */

  $scope.$watchCollection('vm.model', (newValue, oldValue) => {
    this.tableStatus = 'Carregando...';
    this.model = newValue;
    this.sort(this.sortKey);
    $timeout(() => {
      if ((this.model || []).length < 1) this.tableStatus = 'Nenhum registro encontrado';
    }, 3000);
  });

  if (!this.columnsVisibility) this.columnsVisibility = {};

  // if (!Object.keys(this.columnsVisibility).length > 0) {
    $scope.$watchCollection('vm.tableHeader', (newValue, oldValue) => {
      Object.keys(newValue).forEach((item, index) => {
        if (!item) return;
        let parsedItem = newValue[item].split(':');
        let isSortable = newValue[item].match(/\!/) ? false : true;
        let isInactiveByDefault = newValue[item].match(/\?/) ? true : false;

        let alignment = newValue[item].split('$');

        this.tableHeaderMap[parsedItem[0]] = {
          id: index,
          path: parsedItem[1] ? parsedItem[1].replace('?', '').replace('!', '') : null,
          name: parsedItem[0],
          sort: isSortable,
          align: alignment[1] ? alignment[1].replace('$', '') : null,
          isActive: !isInactiveByDefault,
          originalAttr: newValue[item]
        };
      });
    }); // $scope.$watchCollection
  // } else {
  //   this.tableHeaderMap = this.columnsVisibility;
  // }

  this.updateColumns = (columnName) => {
    const fakeHeaderMap = angular.copy(this.tableHeaderMap, {});
    fakeHeaderMap[columnName].isActive = !fakeHeaderMap[columnName].isActive;

    const numberOfCols = Object.keys(fakeHeaderMap).length;
    const checkCols = Object.keys(fakeHeaderMap).filter((item) => fakeHeaderMap[item].isActive);

    if (checkCols.length == 0) return;

    this.tableHeaderMap[columnName].isActive = !this.tableHeaderMap[columnName].isActive;

    if (this.onChangeColumns) this.onChangeColumns(this.tableHeaderMap);
    $scope.$broadcast('change.cols');
  };


  // Export functions
  this.exportPDF = () => exportToPDF('pdf-table', this.tableHeaderMap);
  this.exportCSV = () => exportToCSV('csv-table', this.tableHeaderMap);

};

function datatableRowDirective() {
  return {
    restrict: 'E',
    scope: {
      rowOnclick: '=',
      rowSelection: '@'
    },
    replace: true,
    transclude: true,
    controller: ($scope, $timeout) => {

      let datatable = $scope.$parent.$parent.vm;

      $scope.tableRows = datatable.model;

      $scope.$watchCollection('$parent.$parent.vm.model', (newValue, oldValue) => {
        $scope.tableRows = newValue;
        $timeout(() => {
          if ((datatable.model || []).length > 0) datatable.tableStatus = null;
        }, 600);
      });

      $scope.$watch('$parent.$parent.vm.limitTo', (newValue) => $scope.limitTo = newValue);

      $scope.$watch('$parent.$parent.vm.sortKey', (newValue) => {
        $scope.sortKey = newValue;
      });

      $scope.$watch('$parent.$parent.vm.reverse', (newValue) => {
        $scope.reverse = newValue;
      });

    },
    templateUrl: () => 'directives/datatable/datatable-row.template.html'
  };
};

function datatableColumnDirective($parse) {
  return {
    restrict: 'E',
    scope: {},
    replace: true,
    transclude: true,
    controller: ($scope, $attrs) => {

      let datatable = $scope.$parent.$parent.$parent.$parent.$parent.vm;

      if (!datatable) return;

      let row = $scope.$parent.$parent.$parent;

      $scope.showOptions = false;

      $scope.$row = $scope.$parent.$parent.tableRow;

      $scope.columnName = $attrs.columnName;

      if ($attrs.columnName.match(/\{\{/)) {
        const regExp = /\{\{([^)]+)\}\}/;
        const extract = regExp.exec($attrs.columnName);
        if (!extract) return $scope.columnName = $attrs.columnName;
        $scope.columnName = $parse(extract[1])($scope);
      }

      let colAttrs = `${
        $scope.columnName}:${
        $attrs.columnPath ? $attrs.columnPath : ''}${
        $attrs.columnConfig ? $attrs.columnConfig : ''
      }${
        $attrs.align ? '$' + $attrs.align : ''
      }`;

      if ($attrs.columnName && !datatable.tableHeader[$scope.columnName]) {
        datatable.tableHeader[$scope.columnName] = colAttrs;
      }

      $scope.align = $attrs.align ? $attrs.align : '';
      $scope.width = $attrs.width ? $attrs.width : '';

      if (!$attrs.columnName && datatable && datatable.showOptions) {
        $scope.showOptions = true;
      }

      $scope.isClickable = (typeof row.rowOnclick === 'function');

      if ($attrs.columnConfig) {
        $scope.isActive = $attrs.columnConfig.match(/\?/) ? false : true;
      } else {
        $scope.isActive = true;
      }

      $scope.rowOnclickHandler = (event) => {
        if (typeof row.rowOnclick === 'function') row.rowOnclick($scope.$row);
      };

      $scope.$on('change.cols', () => {
        if (datatable.tableHeaderMap[$scope.columnName]) {
          $scope.isActive = datatable.tableHeaderMap[$scope.columnName].isActive;
        }
      });

      if (datatable.tableHeaderMap[$scope.columnName]) {
        $scope.isActive = datatable.tableHeaderMap[$scope.columnName].isActive;
      }

      $scope.$watchCollection('$parent.$parent.tableRow', (newValue, oldValue) => {
        $scope.$row = newValue;
      });

    },
    templateUrl: () => 'directives/datatable/datatable-column.template.html'
  };
};

function colWrapperDirective() {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    controller: ($scope, $attrs) => {
      $scope.$row = $scope.$parent.$row;
      $scope.$watchCollection('$parent.$row', (newValue, oldValue) => {
        $scope.$row = newValue;
      });
    },
    template: `<div><ng-transclude></ng-transclude></div>`
  };
};

function toExportOnlyDirective() {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    template: `<div data-to-export-only="true" style="display: none;"><ng-transclude></ng-transclude></div>`
  };
};
