import './';

describe('modal basic tests', () => {

  let $compile, $rootScope;

  beforeEach(angular.mock.module('modal'));

  beforeEach(angular.mock.inject((_$compile_, _$rootScope_) => {
    $compile = _$compile_;
    $rootScope = _$rootScope_;
  }));

  let element;

  beforeEach(() => {
    $rootScope.callback = () => {};
    $rootScope.isOpen = false;
    element = $compile(`
      <modal title="Teste da modal" is-active="isOpen" on-close="callback">
        <include include-src="./app/routes/root/templates/modal.html"></include>
      </modal>
    `)($rootScope);
    $rootScope.$digest();
  });

  it('Abre a modal quando is-active for setado como "true".', () => {
    $rootScope.isOpen = true;
    $rootScope.$digest();
    expect(element[0].classList.contains('is-active')).toBe(true);
  });

  it('Fecha a modal quando is-active for setado como "false".', () => {
    $rootScope.isOpen = false;
    $rootScope.$digest();
    expect(element[0].classList.contains('is-active')).toBe(false);
  });

});
