function provider($rootScope, $timeout) {
  return {

    open: (config) => {
      $timeout(() => {
        $rootScope.$broadcast('$modal.open', config);
      });
    },

    close: () => $timeout(() => {
      $rootScope.$broadcast('$modal.close');
    }),

    block: (status, config) => {
      $rootScope.$broadcast('$modal.block', status, config);
    },

    unBlock: () => {
      $rootScope.$broadcast('$modal.unblock');
    }

  };
}

export default provider;
