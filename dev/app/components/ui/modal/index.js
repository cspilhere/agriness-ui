/**
 * @namespace @directive [ui] modal
 */

import 'angular';

import provider from './provider';

angular.module('modal', [])
.directive('modal', directive)
.factory('$modal', provider)
.run(template);

function directive($templateCache) {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    scope: {
      isActive: '=?isActive',

      title: '@?title',
      hideCloseButton: '=?hideCloseButton',
      onClose: '=?onClose',
      type: '@?type',
      size: '@?size',

      status: '@?status'
    },
    controller: controller,
    controllerAs: 'vm',
    bindToController: true,
    template: $templateCache.get('components/ui/modal')
  };
};

function controller($scope, $element, $timeout, $modal) {

  // Setup inicial
  this.title = 'Modal';
  this.hideCloseButton = false;
  this.isActive = true;

  // Abre a modal
  this.openModal = (state, config = {}) => {
    if (state) updateModalPosition($element[0]);
    if (config.hasOwnProperty('title')) this.title = config.title;
    if (config.hasOwnProperty('hideCloseButton')) this.hideCloseButton = config.hideCloseButton;
    if (config.hasOwnProperty('onClose')) this.onClose = config.onClose;
    if (config.hasOwnProperty('size')) this.size = config.size;
    if (config.hasOwnProperty('type')) this.type = config.type;
    this.isActive = state;
  };

  // Fecha a modal
  this.closeModal = () => {
    $modal.config = null;
    this.isActive = false;
    this.status = null;
    this.statusMessage = null;
    this.onClose ? this.onClose() : null;
  };

  // Remove o bloqueio da modal
  this.unblockModal = () => {
    $modal.statusConfig = null;
    this.status = null;
    this.statusMessage = null;
    this.onLeave = null;
    this.onStay = null;
  };

  // Executa os respectivos callbacks quando o usuário clica em sair ou ficar no bloqueio da modal
  this.modalStatusHandler = (decision) => {
    if (decision == 'leave') this.onLeave ? this.onLeave(decision) : null;
    if (decision == 'stay') this.onStay ? this.onStay(decision) : null;
  };

  // Toggle modal
  $scope.$watch('vm.isActive', (newValue) => {
    this.openModal(newValue);
  });


  // Listeners

  $scope.$on('$modal.open', (event, config) => {
    $modal.config = config;
    this.openModal(true, config);
  });

  $scope.$on('$modal.close', () => {
    this.closeModal();
  });

  $scope.$on('$modal.block', (event, status, config) => {
    $modal.statusConfig = config;
    this.status = status;
    this.statusMessage = config.statusMessage || null;
    this.onLeave = typeof config.onLeave == 'function' ? config.onLeave : this.closeModal;
    this.onStay = typeof config.onStay == 'function' ? config.onStay : this.unblockModal;
    $scope.$apply();
  });

  $scope.$on('$modal.unblock', () => {
    this.unblockModal();
  });

};

function template($templateCache) {
  return (
    $templateCache.put('components/ui/modal', `
      <div
        class="modal"
        ng-class="{
          'is-active': vm.isActive
        }"
        ng-keydown="vm.keyPress($event.which)">

        {{vm.status + 'foo'}}

        <div
          class="modal__content"
          ng-class="vm.type == 'danger' ? 'modal__content--danger' : ''">

          <div
            class="modal-alert modal-alert--loading"
            ng-class="{
              'modal-alert--small': vm.size == 'small'
            }"
            ng-show="vm.status == 'working'">
            <div class="modal-alert__icon-loading">
              <svg viewBox="0 0 20.637507 26.193751" id="logo-element" class="icon"><path stroke-width=".367" d="M11.451.132h9.222l-11.35 26.07H.101z"></path></svg>
            </div>
            <span class="modal-alert__title">Estou Processando!</span>
            <span class="modal-alert__description">{{vm.statusMessage}}</span>
            <div class="modal-alert__footer">
              <button
                type="button"
                tabindex="1"
                ng-click="vm.modalStatusHandler('leave')"
                class="button button--outline button--medium">
                <span class="button__label">Cancelar</span>
              </button>
            </div>
          </div>

          <div
            class="modal-alert modal-alert--success"
            ng-class="{
              'modal-alert--small': vm.size == 'small'
            }"
            ng-show="vm.status == 'success'">
            <div class="modal-alert__icon">
              <svg id="check" viewBox="0 0 24 24" class="icon"><path class="alst0" d="M12 2C6.5 2 2 6.5 2 12s4.5 10 10 10 10-4.5 10-10S17.5 2 12 2zm0 18c-4.4 0-8-3.6-8-8s3.6-8 8-8 8 3.6 8 8-3.6 8-8 8z"></path><path class="alst0" d="M10.6 13.5l-3-2.2-1.5 2 4.9 3.6 6.6-8.3-1.9-1.5z"></path></svg>
            </div>
            <span class="modal-alert__title">Bom trabalho!</span>
            <span class="modal-alert__description">{{vm.statusMessage}}</span>
            <div class="modal-alert__footer">
              <button
                type="button"
                tabindex="1"
                ng-click="vm.modalStatusHandler('leave')"
                class="button button--success button--outline button--medium">
                <span class="button__label">Sair</span>
              </button>
              <button
                type="button"
                tabindex="1"
                ng-click="vm.modalStatusHandler('stay')"
                class="button button--success button--medium">
                <span class="button__label">Continuar</span>
              </button>
            </div>
          </div>

          <div
            class="modal-alert modal-alert--danger"
            ng-class="{
              'modal-alert--small': vm.size == 'small'
            }"
            ng-show="vm.status == 'error'">
            <div class="modal-alert__icon">
              <svg id="error" viewBox="0 0 24 24" class="icon"><path class="bcst0" d="M21.9 20.5l-9-18c-.3-.7-1.5-.7-1.8 0l-9 18c-.2.3-.1.7 0 1 .2.3.6.5.9.5h18c.3 0 .7-.2.8-.5.2-.3.2-.7.1-1zM4.6 20L12 5.2 19.4 20H4.6z"></path><path class="bcst0" d="M11 10h2v6h-2z"></path><circle class="bcst0" cx="12" cy="18" r="1.3"></circle></svg>
            </div>
            <span class="modal-alert__title">Ops! Ocorreu um erro!</span>
            <span class="modal-alert__description">{{vm.statusMessage}}</span>
            <div class="modal-alert__footer">
              <button
                type="button"
                tabindex="1"
                ng-click="vm.modalStatusHandler('leave')"
                class="button button--danger button--outline button--medium">
                <span class="button__label">Sair</span>
              </button>
              <button
                type="button"
                tabindex="1"
                ng-click="vm.modalStatusHandler('stay')"
                class="button button--danger button--medium">
                <span class="button__label">Ficar e corrigir</span>
              </button>
            </div>
          </div>

          <header class="modal__header">
            <h1 class="heading heading--regular">{{vm.title}}</h1>
            <button ng-click="vm.closeModal()" ng-if="!vm.hideCloseButton">
              <i class="fa fa-times" aria-hidden="true"></i>
            </button>
          </header>

          <div class="modal__body">
            <ng-transclude></ng-include>
          </div>

        </div>

        <div class="modal-background" ng-click="vm.closeModal()" ng-if="!vm.hideCloseButton"></div>

        <div class="modal-background" ng-if="vm.hideCloseButton"></div>

      </div>
    `)
  );
}

function elementProps(target) {
  if (!target) return;
  let box = target.getBoundingClientRect();
  let body = document.body;
  let docElem = document.documentElement;
  let scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop;
  let scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;
  let clientTop = docElem.clientTop || body.clientTop || 0;
  let clientLeft = docElem.clientLeft || body.clientLeft || 0;
  let top  = box.top +  scrollTop - clientTop;
  let left = box.left + scrollLeft - clientLeft;
  return { top: Math.round(top), left: Math.round(left), width: box.width, height: box.height };
};

const windowSize = function() {
  let wWidth = window.innerWidth;
  let wHeight = window.innerHeight;
  return { width: wWidth, height: wHeight };
};

function updateModalPosition(element) {
  let modalContent = element.querySelector('.modal__content');
  let modalContentTop = Math.round((windowSize().height / 2) - (elementProps(modalContent).height / 2));

  if (elementProps(modalContent).height > windowSize().height || elementProps(modalContent).height == windowSize().height) {
    modalContentTop = 0;
  }

  let modalContentLeft = Math.round((windowSize().width / 2) - (elementProps(modalContent).width / 2));
  modalContent.setAttribute('style', 'top: ' + modalContentTop + 'px; left: ' + modalContentLeft + 'px;')
}
