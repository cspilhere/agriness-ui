/**
 * @namespace @directive [ui] avatar
 * @description Componente de mídia composto por uma figura, título e descrição.
 * @property {string} [label] Título da figura, nome do usuário, etc
 * @property {string} [subtext] Descrição da figura, email, cargo, etc
 * @property {string} [picture] Imagem do avatar, quando não houver, o componente irá utilizar as iniciais do label
 * @property {string} [status] Mostra um indicador de status
 * @property {string} [size] Configura o tamanho do componente (large)
 * @property {string} [style] Configura um estilo (stacked)
 * @property {boolean} [hide-description] Esconde a descrição
 * @example
 * <avatar
 *  label="Senhor do Mar"
 *  subtext="domar@gmail.com"
 *  picture="#"
 *  size="large"
 *  style="stacked">
 * </avatar>
 *
 * <avatar
 *  label="Senhor do Mar"
 *  subtext="domar@gmail.com"
 *  picture="#">
 * </avatar>
 */

import 'angular';

angular
.module('avatar', [])
.directive('avatar', directive)
.run(($templateCache) => (
  $templateCache.put('components/avatar', `
    <div class="media" ng-class="{
        'media--stacked': vm.style == 'stacked',
        'media--large': vm.size == 'large'
      }">
      <span class="media__icon">
        <div class="avatar" ng-class="{
          'avatar--online': vm.status == 'online',
          'avatar--away': vm.status == 'away',
          'avatar--busy': vm.status == 'busy',
          'avatar--unknown': vm.status == 'unknown',
          'avatar--large': vm.size == 'large'
        }">
          <span class="avatar__picture" ng-if="vm.picture">
            <img ng-src="{{vm.picture}}" alt="{{vm.label || vm.subtext}}" />
          </span>
          <span class="avatar__placeholder" ng-if="!vm.picture">
            {{vm.label.split(' ')[0][0] + vm.label.split(' ')[vm.label.split(' ').length - 1][0]}}
          </span>
        </div>
      </span>
      <div class="media__description" ng-if="vm.hideDescription != true">
        <p class="text text--xsmall text--semibold">{{vm.label}}</p>
        <p class="text text--xsmall text--light">{{vm.subtext}}</p>
      </div>
    </div>
  `)
));

function directive($templateCache) {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    require: ['ngModel'],
    scope: {
      hideDescription: '=?hideDescription',
      label: '@',
      subtext: '@',
      picture: '@',
      status: '@',
      size: '@',
      style: '@'
    },
    controller: controller,
    controllerAs: 'vm',
    bindToController: true,
    template: $templateCache.get('components/avatar')
  };
};

function controller($scope) {};
