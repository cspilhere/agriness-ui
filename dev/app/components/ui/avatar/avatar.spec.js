import './';

describe('avatar basic tests', () => {

  let $compile, $rootScope;

  beforeEach(angular.mock.module('avatar'));

  beforeEach(angular.mock.inject((_$compile_, _$rootScope_) => {
    $compile = _$compile_;
    $rootScope = _$rootScope_;
  }));

  it('Mostra o placeholder quando a imagem não é informada.', () => {
    let element = $compile(`
      <avatar
      label="Senhor do Mar"
      subtext=""
      picture="">
    `)($rootScope);
    $rootScope.$digest();
    expect(element[0].querySelector('.avatar__placeholder').textContent).toContain('SM');
  });

});
