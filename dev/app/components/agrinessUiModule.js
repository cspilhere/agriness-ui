import 'angular';

import 'angular-translate';

import Mask from '../services/masksAndValidators/dataMask';
import Validator from '../services/masksAndValidators/dataValidator';
import ParseValue from '../services/masksAndValidators/ParseValue';

import './base/appContent';
import './base/appWrapper';
import './base/appHeader';
import './base/appFooter';
import './base/mainContainer';
import './base/notifications';
import './base/userMenu';
import './base/pageTitle';
import './base/breadcrumbs';
import './base/appLoading';
import './base/sidebar';
import './base/inlineNavigation';

import './ui/avatar';
import './ui/grid';
import './ui/alert';
import './ui/buttonComponent';
import './ui/labelComponent';
import './ui/icon';
import './ui/text';
import './ui/row';

import './ui/container';
import './ui/modal';
import './ui/tabs';
import './ui/datatable';

import './ui/formfieldWrapper';
import './ui/formfieldText';
import './ui/formfieldSelect';
import './ui/switchFieldDateDays';
import './ui/switchFieldWeight';

import './ui/card';

import './utils/toggle';
import './utils/include';

// import './ui/carousel';

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module unless amdModuleId is set
    define([], function () {
      return (factory());
    });
  } else if (typeof module === 'object' && module.exports) {
    // Node. Does not work with strict CommonJS, but
    // only CommonJS-like environments that support module.exports,
    // like Node.
    module.exports = factory();
  } else {
    factory();
  }
}(this, function () {

  angular.module('agriness-ui', [
    'appContent',
    'appWrapper',
    'appHeader',
    'appFooter',
    'mainContainer',
    'notifications',
    'userMenu',
    'pageTitle',
    'breadcrumbs',
    'appLoading',
    'sidebar',
    'inlineNavigation',

    'avatar',
    'grid',
    'alert',
    'buttonComponent',
    'labelComponent',
    'icon',
    'text',
    'row',

    'container',
    'modal',
    'tabs',
    'datatable',

    'formfieldWrapper',
    'formfieldText',
    'formfieldSelect',
    'switchFieldDateDays',
    'switchFieldWeight',

    'card',

    'toggle',
    'include',

    // 'carousel',

    'pascalprecht.translate'
  ])
  .service('Mask', Mask)
  .service('Validator', Validator)
  .service('ParseValue', ParseValue)
  .config(['$translateProvider', ($translateProvider) => {

    $translateProvider.translations('pt-br', {
      'logout': 'Sair'
    });

    $translateProvider.translations('es', {
      'logout': 'Salir'
    });

    $translateProvider.translations('en', {
      'logout': 'Logout'
    });

    let lang = navigator.language || navigator.userLanguage;
    let langIndex = { 'en-US': 'en', 'en': 'en', 'es-ES': 'es', 'es': 'es', 'pt-BR': 'pt-br', 'pt': 'pt-br' };

    $translateProvider.preferredLanguage(langIndex[lang]);

  }]);

}));


