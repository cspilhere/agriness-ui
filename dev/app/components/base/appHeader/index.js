/**
 * @namespace @directive [layout] app-header
 * @description Cabeçalho principal da aplicação, quando configurado, contém um mega menu.
 * @property {string} brand Texto que representa a marca da aplicação
 * @property {string} [default-title] Título padrão para iniciar quando não há um módulo selecionado
 * @property {array} [menu-tree] Lista de módulos
 * @example
 * <app-wrapper>
 *   <app-header brand="365" default-title="My Account" menu-tree="[{...}]">
 *     Outras diretivas
 *   </app-header>
 * </app-wrapper>
 */

import 'angular';

angular
.module('appHeader', [])
.directive('appHeader', directive)
.run(($templateCache, $http) => (
  $templateCache.put('components/base/app-header', `
    <header class="main-header" role="banner">

      <a href="#main" title="Ir para conteúdo principal" class="u-text-assistive" tabindex="0">
        Ir para conteúdo principal
      </a>

      <div class="main-header__wrapper">

        <div class="mega-menu" data-toggle-id="{{vm.menuTree ? 'appHeaderTrigger' : 'none'}}">

          <a>
            <div class="mega-menu__trigger" toggle toggle-target="appHeaderTrigger">
              <div class="main-header__brand">
                <h1 class="heading heading--inverse" style="font-size: 30px;">
                  {{vm.brand}}
                  <img ng-src="{{vm.brandImage}}" height="26" ng-if="vm.brandImage" />
                </h1>
              </div>
              <div class="mega-menu__icon" ng-if="vm.menuTree">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon">
                  <use xlink:href="./media/sprites/sprite.defs.svg#menu"></use>
                </svg>
              </div>
              <span class="mega-menu__label">
                {{vm.currentModule.name || vm.defaultTitle}}
              </span>
            </div>
          </a>

          <div class="mega-menu__content">
            <div class="mega-menu__container">
              <div class="grid grid--gutter-xl grid--align-center">

                <div class="grid__col grid__col--lg-2" ng-repeat="module in vm.menuTree">
                  <div class="blurb">
                    <a ng-click="vm.goto(module.path, module)">
                      <header class="blurb__header">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon--inverse icon--rounded icon--small">
                          <use xlink:href="./media/sprites/sprite.defs.svg#{{module.icon}}"></use>
                        </svg>
                        <h3 class="heading heading--inverse heading--xsmall heading--uppercase heading--semibold">
                          {{module.name}}
                        </h3>
                      </header>
                    </a>
                    <hr class="blurb__divisor">
                    <div class="blurb__body">
                      <nav class="mega-menu-nav">
                        <ul>
                          <li ng-repeat="link in module.children">
                            <a ng-click="vm.goto(link.path, module, link.params)">{{link.name}}</a>
                          </li>
                        </ul>
                      </nav>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>

        <hr class="vr vr--light">

        <div class="apps-navigation">
          <span ng-if="vm.label">{{vm.label}}</span>
          <apps-navigation data="vm.currentModule.apps" ng-if="!vm.label"></apps-navigation>
        </div>

        <hr class="vr vr--light">

        <div class="main-header__tools" ng-transclude></div>

      </div>

    </header>
  `)
));

function directive($templateCache) {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    scope: {
      menuTree: '=',
      brand: '@',
      brandImage: '@',
      defaultTitle: '@',
      label: '@'
    },
    controller: controller,
    controllerAs: 'vm',
    bindToController: true,
    template: $templateCache.get('components/base/app-header')
  };
};

function controller($scope, $state, $timeout) {

  this.currentModule = {};

  $scope.$watch('vm.menuTree', (newValue) => {
    (newValue || []).forEach((module) => {
      $timeout(() => {
        if ($state.current.name.match(module.path)) this.currentModule = module;
      }, 100);
    });
  });

  this.goto = (path, module, params) => {
    this.currentModule = module;
    $state.go(path, params || {});
    $timeout(() => {
      document.querySelector('[data-toggle-id="appHeaderTrigger"]').classList.remove('is-active');
      document.querySelector('[toggle-target="appHeaderTrigger"]').classList.remove('is-active');
    }, 150);
  };

};
