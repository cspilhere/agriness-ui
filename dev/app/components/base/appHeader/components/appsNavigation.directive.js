import s3Air from 's3Air';

s3Air
.directive('appsNavigation', directive)
.run(($templateCache, $http) => (
  $templateCache.put('components/base/apps-navigation', `
    <nav class="menu" role="navigation">
      <ul class="menu__content">
        <li
          class="menu__item"
          ng-repeat="app in vm.data"
          profile="app.profile"
          ng-class="{ 'is-active': vm.currentApp == app.name }">
          <a ng-click="vm.goto(app.path, app.name, app.params)">
            {{app.name}}
          </a>
        </li>
      </ul>
    </nav>
  `)
));

function directive($templateCache) {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    scope: {
      data: '='
    },
    controller: controller,
    controllerAs: 'vm',
    bindToController: true,
    template: $templateCache.get('components/base/apps-navigation')
  };
};

function controller($scope, $state, $timeout) {

  this.currentApp = null;

  $scope.$watch('vm.data', (newValue) => {
    (newValue || []).forEach((app) => {
      $timeout(() => {
        if ($state.current.name.match(app.path)) this.currentApp = app.name;
      }, 100);
    });
  });

  this.goto = (path, name, params) => {
    this.currentApp = name;
    $state.go(path, params || {});
  };

};
