/**
 * @namespace @directive [layout] app-content
 * @description Wrapper do conteúdo principal da aplicação.
 * @example
 * <app-wrapper>
 *   <app-content>
 *     Conteúdo!
 *   </app-content>
 * </app-wrapper>
 */

import 'angular';

angular
.module('appContent', [])
.directive('appContent', directive)
.run(($templateCache, $http) => (
  $templateCache.put('components/base/app-content', `
    <main class="main" data-toggle-id="sidebar" ng-transclude></main>
  `)
));

function directive($templateCache) {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    scope: {},
    controller: controller,
    controllerAs: 'vm',
    bindToController: true,
    template: $templateCache.get('components/base/app-content')
  };
};

function controller($scope) {};
