/**
 * @namespace @directive [ui] user-menu
 * @description Componente específico composto pelas informações do usuário, container (transclude) e botão de logout.
 * @property {object} user-data Objeto com as informações do usuário contendo as chaves: name{string}, email{string} e picture{string}
 * @property {function} on-click-logout Callback que executa quando o usuário clica em sair/logout
 * @example
 * <user-menu user-data="{}" on-click-logout="callback">
 *  + opções
 * </user-menu>
 */

import 'angular';

angular
.module('userMenu', [])
.directive('userMenu', directive)
.run(($templateCache, $http) => (
  $templateCache.put('components/base/user-menu', `
    <div class="dropdown dropdown--specialpadding is-last dropdown--light" data-toggle-id="userMenu">

      <a class="dropdown__header" toggle toggle-target="userMenu" toggle-click-inside="true">
        <avatar
          label="{{vm.userData.name}}"
          subtext="{{vm.userData.email}}"
          picture="{{vm.userData.picture}}"
          status="online"
          hide-description="true">
        </avatar>
      </a>

      <div class="dropdown__content dropdown__content--fixed">

        <div class="user-menu">

          <div class="user-menu__header">
            <avatar
              label="{{vm.userData.name}}"
              subtext="{{vm.userData.email}}"
              picture="{{vm.userData.picture}}">
            </avatar>
          </div>

          <div class="user-menu__body" ng-transclude></div>

          <div class="user-menu__footer">
            <a ng-click="vm.onClickLogout()">
              <div class="media">
                <span class="media__icon">
                  <i class="icon-sair"></i>
                </span>
                <span class="media__text">
                  <p class="text text--default text--semibold">{{'logout' | translate}}</p>
                </span>
              </div>
            </a>
          </div>

        </div>

      </div>
    </div>
  `)
));

function directive($templateCache) {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    scope: {
      userData: '=',
      onClickLogout: '='
    },
    controller: controller,
    controllerAs: 'vm',
    bindToController: true,
    template: $templateCache.get('components/base/user-menu')
  };
};

function controller($scope) {};
