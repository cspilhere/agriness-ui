/**
 * @namespace @directive [layout] app-footer
 * @description Rodapé principal da aplicação, não é configurável e contém apenas informações de contato da Agriness.
 * @example
 * <app-wrapper>
 *   <app-footer></app-footer>
 * </app-wrapper>
 */

import 'angular';

angular
.module('appFooter', [])
.directive('appFooter', directive)
.run(($templateCache, $http) => (
  $templateCache.put('components/base/app-footer', `
    <footer class="main-footer" style="width: 100%; bottom: 0;">

      <div class="main-footer__wrapper">
        <div class="main-footer__brand">
          <span class="image-container">
            <!-- <img src="" alt="Agriness" /> -->
          </span>
        </div>
        <div class="main-footer__content">
          <p class="footer-info">
            <span class="phone">+55 (48) 3028 0015</span> | <a href="http://www.agriness.com/pt/" target="_blank">agriness.com</a>
          </p>
        </div>
      </div>

    </footer>
  `)
));

function directive($templateCache) {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    scope: {},
    controller: controller,
    controllerAs: 'vm',
    bindToController: true,
    template: $templateCache.get('components/base/app-footer')
  };
};

function controller($scope) {};
