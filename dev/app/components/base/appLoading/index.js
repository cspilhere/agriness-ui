/**
 * @namespace @directive [layout] app-loading
 * @description Bloqueia a tela enquanto a aplicação está sendo preparada.
 * @property {boolean} [is-loaded] Indica quando a aplicação está carregada e o bloqueio de tela deverá ser suspenso
 * @example
 * <app-loading is-loaded="true/false"></app-loading>
 * <app-wrapper>...</app-wrapper>
 */

import 'angular';

angular
.module('appLoading', [])
.directive('appLoading', directive)
.run(($templateCache) => (
  $templateCache.put('components/base/app-loading', `
    <span>
      <div class="splash-screen" ng-if="!vm.isLoaded">
        <div class="modal-alert__icon-loading">
          <svg viewBox="0 0 20.637507 26.193751" id="logo-element" class="icon"><path stroke-width=".367" d="M11.451.132h9.222l-11.35 26.07H.101z"></path></svg>
        </div>
      </div>
    </span>
  `)
));

function directive($templateCache) {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    scope: {
      isLoaded: '=?isLoaded'
    },
    controller: controller,
    controllerAs: 'vm',
    bindToController: true,
    template: $templateCache.get('components/base/app-loading')
  };
};

function controller($scope) {};
