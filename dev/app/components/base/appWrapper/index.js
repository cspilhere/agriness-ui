/**
 * @namespace @directive [layout] app-wrapper
 * @description Wrapper da aplicação.
 * @example
 * <app-wrapper>
 *   ...
 * </app-wrapper>
 */

import 'angular';

angular
.module('appWrapper', [])
.directive('appWrapper', directive)
.run(($templateCache, $http) => (
  $templateCache.put('components/base/app-wrapper', `
    <div class="app-wrapper" ng-transclude></div>
  `)
));

function directive($templateCache) {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    scope: {},
    controller: controller,
    controllerAs: 'vm',
    bindToController: true,
    template: $templateCache.get('components/base/app-wrapper')
  };
};

function controller($scope) {};
