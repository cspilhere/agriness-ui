import 'angular';

angular
.module('notifications', [])
.directive('notifications', directive)
.run(($templateCache, $http) => (
  $templateCache.put('components/base/notifications', `
    <div></div>
  `)
));

function directive($templateCache) {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    scope: {},
    controller: controller,
    controllerAs: 'vm',
    bindToController: true,
    template: $templateCache.get('components/base/notifications')
  };
};

function controller($scope) {};
