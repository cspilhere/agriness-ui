/**
 * @namespace @directive [ui] sidebar
 * @description Sidebar para navegação.
 * @property {string} [link] (sidebar-item) Url executada via location.href
 * @property {string} [icon-name] (sidebar-item) Nome do ícone mostrado ao lado esquerdo do texto
 * @property {function} on-click (sidebar-item) Callback que recebe como parâmetro o valor da propriedad "link" e quando configurado, anula o location.href
 * @example
 * <sidebar>
 *  <sidebar-item link="#" icon-name="tags" on-click="clickSidebar">
 *    Link 1
 *  </sidebar-item>
 *  <sidebar-item link="#" icon-name="binoculars" active="true">
 *    Link 2
 *  </sidebar-item>
 * </sidebar>
 */

import 'angular';

angular
.module('sidebar', [])
.directive('sidebar', directive)
.run(template)
.directive('sidebarItem', sidebarItemDirective);

function template($templateCache) {
  $templateCache.put('components/base/sidebar', `
    <div class="sidebar-wrapper">
      <!--<a class="sidebar__toggle" toggle toggle-click-outside="false" toggle-target="sidebar">
        <i class="fa fa-angle-double-right" aria-hidden="true"></i>
      </a>-->
      <div class="sidebar">
        <ul class="sidebar__list" style="margin-top: 1px;">
          <ng-transclude></ng-transclude>
        </ul>
      </div>
    </div>
  `);
};

function directive() {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    scope: {},
    controller: controller,
    controllerAs: 'vm',
    bindToController: true,
    templateUrl: 'components/base/sidebar'
  };
};

function controller() {};

function sidebarItemDirective() {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    scope: {
      link: '@',
      iconName: '@',
      active: '=',
      onClick: '='
    },
    controller: ($scope, $state, $rootScope) => {
      $scope.vm.goto = (link) => {
        if (typeof $scope.vm.onClick == 'function') {
          $scope.vm.onClick(link);
        } else {
          location.href = link;
        }
      };
    },
    controllerAs: 'vm',
    bindToController: true,
    template: `
      <li
        class="sidebar__list-item"
        ng-class="{ 'is-active': vm.active }">
        <a ng-click="vm.goto(vm.link)">
          <div class="sidebar__list-icon">
            <i class="fa fa-{{vm.iconName}}" title="" aria-hidden="true"></i>
          </div>
          <span class="sidebar__list-label" title="">
            <ng-transclude></ng-transclude>
          </span>
        </a>
      </li>
    `
  };
};
