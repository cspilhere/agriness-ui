import 'angular';

angular
.module('mainContainerBody', [])
.directive('mainContainerBody', directive)
.run(($templateCache) => (
  $templateCache.put('components/base/main-container-body', `
    <div class="main-container__body" ng-transclude></div>
  `)
));

function directive($templateCache) {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    scope: {},
    controller: controller,
    controllerAs: 'vm',
    bindToController: true,
    template: $templateCache.get('components/base/main-container-body')
  };
};

function controller($scope, $timeout) {};
