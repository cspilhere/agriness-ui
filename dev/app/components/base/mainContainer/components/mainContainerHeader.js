import 'angular';

angular
.module('mainContainerHeader', [])
.directive('mainContainerHeader', directive)
.run(($templateCache) => (
  $templateCache.put('components/base/main-container-header', `
    <div class="main-container__header" ng-transclude></div>
  `)
));

function directive($templateCache) {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    scope: {},
    controller: controller,
    controllerAs: 'vm',
    bindToController: true,
    template: $templateCache.get('components/base/main-container-header')
  };
};

function controller($scope, $timeout) {};
