/**
 * @namespace @directive [ui] main-container
 * @description Container que abriga uma view contendo o conteúdo útil da apicação.
 * @example
 * <main-container>
 *
 *  <main-container-header>
 *    Header
 *  </main-container-header>
 *
 *  <main-container-body>
 *    Content
 *  </main-container-body>
 *
 * </main-container>
 */

import 'angular';

import './components/mainContainerHeader'
import './components/mainContainerBody'

angular
.module('mainContainer', ['mainContainerHeader', 'mainContainerBody'])
.directive('mainContainer', directive)
.run(($templateCache, $http) => (
  $templateCache.put('components/base/main-container', `
    <div class="main-container">
      <div class="main-container__content">

        <ng-transclude ng-transclude-slot="header"></ng-transclude>

        <ng-transclude ng-transclude-slot="body"></ng-transclude>

      </div>
    </div>
  `)
));

function directive($templateCache) {
  return {
    restrict: 'E',
    replace: true,
    transclude: {
      header: '?mainContainerHeader',
      body: '?mainContainerBody'
    },
    scope: {},
    controller: controller,
    controllerAs: 'vm',
    bindToController: true,
    template: $templateCache.get('components/base/main-container')
  };
};

function controller($scope) {};
