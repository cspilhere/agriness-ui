/**
 * @namespace @directive [ui] inline-navigation
 * @description Renderiza um breadcrumbs que indica o caminho percorrido pela navegação.
 * @property {string} name O nome identificador da navegação
 * @property {array} links Array de objetos que configuram a navegação com as chaves: name{string}, value{any}, checked{boolean}
 * @property {function} on-click Callback que irá receber o objeto referente ao item clicado
 * @example
 * <inline-navigation
 *  name="name"
 *  links="[{}]"
 *  on-click="clickInlineNavigation">
 * </inline-navigation>
 */

 import 'angular';

angular
.module('inlineNavigation', [])
.directive('inlineNavigation', directive)
.run(($templateCache) => (
  $templateCache.put('components/base/inline-navigation', `
    <div class="form-component form-component--inline-navigation">
      <label class="button-radio" for="{{item.name + $index}}" ng-repeat="item in vm.links">
        <input
          id="{{item.name + $index}}"
          type="radio"
          tabindex="1"
          name="{{vm.name}}"
          value="{{item.value}}"
          ng-model='model'
          ng-change="null"
          ng-click="vm.onClick(item)"
          ng-checked="item.checked">
        <span class="button-radio__token">
          <span class="button-radio__label">{{item.name}}</span>
        </span>
      </label>
    </div>
  `)
));

function directive($templateCache) {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    scope: {
      name: '@',
      links: '=',
      onClick: '='
    },
    controller: controller,
    controllerAs: 'vm',
    bindToController: true,
    template: $templateCache.get('components/base/inline-navigation')
  };
};

function controller($scope) {};
