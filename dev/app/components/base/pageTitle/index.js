/**
 * @namespace @directive [ui] page-title
 * @description Mostra o título principal da página/view/tela que o usuário se encontra
 * @property {string} title
 * @example
 * <page-title title="Demo"></page-title>
 */

import 'angular';

angular
.module('pageTitle', [])
.directive('pageTitle', directive)
.run(($templateCache) => (
  $templateCache.put('components/base/page-title', `
    <header class="main-container__header">
      <h1 class="heading heading--medium">{{vm.title}}</h1>
      <ng-transclude></ng-transclude>
    </header>
  `)
));

function directive($templateCache) {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    scope: {
      title: '@'
    },
    controller: controller,
    controllerAs: 'vm',
    bindToController: true,
    template: $templateCache.get('components/base/page-title')
  };
};

function controller($scope) {};
