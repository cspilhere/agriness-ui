/**
 * @namespace @directive [ui] breadcrumbs
 * @description Renderiza um breadcrumbs que indica o caminho percorrido pela navegação.
 * @property {expression} [path] Array de strings
 * @example
 * <breadcrumbs path="['Home', 'Demo']"></breadcrumbs>
 */

import 'angular';

angular
.module('breadcrumbs', [])
.directive('breadcrumbs', directive)
.run(($templateCache) => (
  $templateCache.put('components/base/breadcrumbs', `
    <nav class="breadcrumbs">
      <ul class="breadcrumbs__list" style="margin-left: 3px; margin-bottom: 6px;">
        <li
          class="breadcrumbs__list-item"
          ng-repeat="item in vm.path"
          ng-class="{ 'is-current': $last }">

          <span class="breadcrumbs__label">
            {{item}}
          </span>

          <span class="breadcrumbs__arrow" ng-if="!$last">
            <i class="fa fa-angle-right" aria-hidden="true"></i>
          </span>

        </li>
      </ul>
    </nav>
  `)
));

function directive($templateCache) {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    scope: {
      path: '='
    },
    controller: controller,
    controllerAs: 'vm',
    bindToController: true,
    template: $templateCache.get('components/base/breadcrumbs')
  };
};

function controller($scope) {};
