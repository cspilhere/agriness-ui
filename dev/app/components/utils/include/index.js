import 'angular';

angular
.module('include', [])
.directive('include', directive);

function directive() {
  return {
    restrict: 'E',
    controller: ($scope, $attrs) => {
      $scope.templateSource = $attrs.includeSrc;
      $scope.includeContentLoaded = () => {};
    },
    template: `<ng-include src="templateSource" onload="includeContentLoaded()"></ng-include>`
  };
};
