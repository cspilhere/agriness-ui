const realPosition = function(target) {
  if (!target) return;
  let box = target.getBoundingClientRect();
  let body = document.body;
  let docElem = document.documentElement;
  let scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop;
  let scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;
  let clientTop = docElem.clientTop || body.clientTop || 0;
  let clientLeft = docElem.clientLeft || body.clientLeft || 0;

  let top = Math.abs(box.top + scrollTop - clientTop);
  let left = Math.abs(box.left + scrollLeft - clientLeft);
  let width = box.width;
  let height = box.height;

  return {
    top: Math.round(Math.abs(box.top)),
    left: Math.round(left),
    width: Math.round(width),
    height: Math.round(height)
  };
};

const viewSizes = function() {
  // Window
  let wWidth = window.innerWidth;
  let wHeight = window.innerHeight;
  // Screen
  let sWidth = screen.width;
  let sHeight = screen.height;
  return { width: wWidth, height: wHeight };
};

let props = null;
let parent = null;

function collision(element) {
  element.removeAttribute('style');
  props = realPosition(element);
  parent = realPosition(element.parentNode);
  if (props.left + props.width > viewSizes().width) {
    element.setAttribute('style', element.getAttribute('style') || '' + 'left: ' + (
      -((props.width - parent.width))
    ) + 'px !important;');
  }
  if (props.top + props.height > viewSizes().height) {
    element.setAttribute('style', element.getAttribute('style')  || '' + 'top: ' + (
      -((props.height))
    ) + 'px !important;');
  }
}

function toggleController($scope, $element, $attrs, $timeout) {

  $scope.element = $element[0];
  $scope.targetElement = document.querySelectorAll('[data-toggle-id="' + $scope.toggleTarget + '"]');
  $scope.eventType = $attrs.toggle || 'click';

  $scope.isActive = false;

  $scope.toggleCollision = $scope.element.getAttribute('toggle-collision');
  $scope.toggleCollisionTarget = document.querySelector('[data-collision-id="' + $scope.toggleCollision + '"]');

  $scope.clickOutside = $scope.element.getAttribute('toggle-click-outside');
  $scope.clickInside = $scope.element.getAttribute('toggle-click-inside');

  $scope.onChangeCallback = () => {
    if (typeof $scope.toggleOnchange === 'function') {
      $scope.toggleOnchange($scope.element.classList.contains('is-active'));
    }
  };

  $scope.$watch('toggleTarget', (newValue, oldValue) => {
    $scope.targetElement = document.querySelectorAll('[data-toggle-id="' + newValue + '"]');
  });

  $element.bind($scope.eventType, () => {
    $scope.isActive = true;
    if (!$scope.element.classList.contains('is-active')) {
      $scope.element.classList.add('is-active');
      if ($scope.toggleCollision) {
        $timeout(() => {
          collision($scope.toggleCollisionTarget);
        }, 100);
      }
      Array(...$scope.targetElement).forEach((item) => {
        item.classList.add('is-active')
      });
      // if($scope.toggleTarget === 'sidebar') {
      //   for(var i = 0, len = document.getElementsByClassName('gridUI').length; i < len; ++i) {
      //     angular.element(document.getElementsByClassName('gridUI')[i]).css('width', '100%');
      //   }
      // }
    } else {
      // if($scope.toggleTarget === 'sidebar') {
      //   let widthGrid = document.getElementsByClassName('main-container__content')[0].clientWidth - document.getElementsByClassName('sidebar')[0].clientWidth - 200;
      //   for(var i = 0, len = document.getElementsByClassName('gridUI').length; i < len; ++i) {
      //     angular.element(document.getElementsByClassName('gridUI')[i]).css('width', widthGrid + 'px');
      //   }
      // }
      $scope.element.classList.remove('is-active');
      Array(...$scope.targetElement).forEach((item) => item.classList.remove('is-active'));
    }
    $timeout(() => $scope.isActive = false, 10);
    $scope.onChangeCallback();
  }); // $element.bind

  $element.bind('focus', () => {
    // Foi comentado pois estava gerando um bug no filtro, caso quebre alguma outra coisa,
    // Falar comigo @cspilhere
    // $scope.isActive = true;
    // $scope.element.classList.add('is-active');
    // Array(...$scope.targetElement).forEach((item) => {
    //   item.classList.add('is-active')
    // });
    // $scope.onChangeCallback();
  }); // $element.bind

  if ($scope.eventType === 'mouseover') {
    $element.bind('mouseout', () => {
      $scope.element.classList.remove('is-active');
      Array(...$scope.targetElement).forEach((item) => item.classList.remove('is-active'));
      $scope.onChangeCallback();
    }); // $element.bind
  }

  if ($scope.clickOutside !== 'false') {
    clickOutside($scope.targetElement, (destroy) => {
      $scope.element.classList.remove('is-active');
      Array(...$scope.targetElement).forEach((item) => item.classList.remove('is-active'));
      $scope.onChangeCallback();
      $scope.destroyListener = destroy;
    });
  }

  if ($scope.clickInside === 'true') {
    Array(...$scope.targetElement).forEach((item) => {
      item.addEventListener('click', (event) => {
        if (!$scope.isActive) {
          $scope.element.classList.remove('is-active');
          item.classList.remove('is-active');
          $scope.onChangeCallback();
        }
      });
    });
  }

  $scope.$on('$destroy', () => $scope.destroyListener ? $scope.destroyListener() : null);

// function clickOutside

  Function.prototype.isNodeList = () => false;
  Element.prototype.isNodeList = () => false;
  NodeList.prototype.isNodeList = HTMLCollection.prototype.isNodeList = () => true;

  function isDescendant(parent, child) {
    var node = child.parentNode;
    while (node != null) {
      if (node == parent) return true;
      node = node.parentNode;
    }
    return false;
  }

  // Path polyfill
  function eventPath(el) {
    var path = [];
    while (el) {
      path.push(el);
      if (el.tagName === 'HTML') {
        path.push(document);
        path.push(window);
        return path;
      }
      el = el.parentElement;
    }
  }

  function clickOutside() {
    let eventCallback = (event) => {
      let parsedArguments = [];
      Array(...arguments).forEach((argument) => {
        if (argument.isNodeList()) {
          Array(...argument).forEach((item) => parsedArguments.push(item));
        } else {
          parsedArguments.push(argument);
        }
      });
      let outside = Array(...parsedArguments).filter((el) => {
        let path = event.path || eventPath(event.target);
        return path.filter((item) => {
          if (item.classList) return el == item || isDescendant(el, item) ? true : false;
        })[0];
      })[0];
      if (typeof arguments[arguments.length - 1] !== 'function') return;
      if (!outside) arguments[arguments.length - 1](destroy);
      function destroy() { document.removeEventListener('click', eventCallback); }
    };
    document.addEventListener('click', eventCallback);
  } // function clickOutside

} // toggleController

export default toggleController;
