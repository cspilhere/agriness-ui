import 'angular';
import controller from './controller';

angular
.module('toggle', [])
.directive('toggle', directive);

function directive() {
  return {
    restrict: 'A',
    scope: {
      toggle: '@',
      toggleTarget: '@',
      toggleClickOutside: '=?toggleClickOutside',
      toggleClickInside: '=?toggleClickInside',
      toggleOnchange: '=',
      toggleOnchangeParams: '='
    },
    controller: controller
  };
};
