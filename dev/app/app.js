import 'angular';
import '@uirouter/angularjs';

const app = angular.module('app', [
  require('angular-cookies'),
  require('angular-sanitize'),
  'ui.router',

  'agriness-ui'

]);

export default app;
