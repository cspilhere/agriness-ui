export const apps = {
  management: // MANEJO
    [{ // visao geral
      label: 'common.overview',
      activeState: 'overview',
      path: 'app.management.overview',
      enabledIn: ['breeding', 'breedingHerd', 'farrowing', 'nursery', 'finishing'],
      profile: '1,2,3,4'
    }, { //lancamento
      label: 'common.animalEvents',
      activeState: 'animalEvents',
      path: 'app.management.animalEvents',
      enabledIn: ['breeding', 'breedingHerd', 'farrowing', 'nursery', 'finishing'],
      profile: '1,2,3,4'
    },{ //central de analise
      label: 'common.reports',
      activeState: 'reports',
      path: 'app.management.reports',
      enabledIn: ['breeding', 'breedingHerd', 'farrowing', 'nursery', 'finishing'],
      profile: '1,2,3,4'
    // }, {
    //   label: 'common.kanban',
    //   activeState: 'kanban',
    //   path: 'kanban'
    }, { // configuracao
      label: 'common.settings',
      activeState: 'settings',
      path: 'app.management.settings',
      enabledIn: ['breeding', 'breedingHerd', 'farrowing', 'nursery', 'finishing'],
      profile: '1,2,3,4'
    }],
  inputs: // INSUMO
    [{ //lancamento
      label: 'common.animalEvents',
      activeState: 'inputEvents',
      path: 'app.inputs.inputEvents',
      enabledIn: ['app.inputs'],
      profile: '1,2,3,4'
    }],
  user:  // MINHA CONTA
    //{Perfil},
    //{Endereços},
    [{ //Preferências
      label: 'common.preferences',
      activeState: 'preferences',
      path: 'app.user.preferences',
      firstItemSide: 'calendar',
      enabledIn: ['app.user'],
      profile: '1,2,3,4'
    }],
  farm: //A GRANJA
    [{ //configuracao
      label: 'common.settings',
      activeState: 'settings',
      path: 'app.farm.settings.setting',
      enabledIn: ['app.farm'],
      profile: '1,2,3,4'
    }, { //configuracao
      label: 'common.audit',
      activeState: 'audit',
      path: 'app.farm.audit',
      enabledIn: ['app.farm'],
      profile: '1,2,3,4'
    }, { //configuracao
      label: 'common.reports',
      activeState: 'reports',
      path: 'app.farm.allReports',
      enabledIn: ['app.farm'],
      profile: '1,2,3,4'
    }],
  account: //GRUPO DE GRANJAS
    [{//granjas
      label: 'common.farms',
      activeState: 'farms',
      path: 'app.account.farms',
      enabledIn: ['app.account'],
      firstItemSide: 'users',
      profile: '1,2,3,4'
    },{ //userAndPermission
      label: 'common.userAndPermission',
      activeState: 'users',
      path: 'app.account.users',
      firstItemSide: 'users',
      enabledIn: ['app.account'],
      profile: '1,2'
    },{ //cadastro
      label: 'common.forms',
      activeState: 'forms',
      path: 'app.account.forms',
      firstItemSide: 'causes',
      enabledIn: ['app.account'],
      profile: '1,2,3,4'
    }]
};
