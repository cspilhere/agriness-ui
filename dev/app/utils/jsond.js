import { deepKeyJson, deserialize, serialize } from 'utils/utils';

class JsonD {

  constructor(string) {
    this.json = deserialize(string);
    if (this.json === undefined || this.json === null) {
      this.json = {};
      console.warn('Invalid string as json, changed for an empty object "{}"');
    }
  }

  get(key) {
    return deepKeyJson(this.json, key);
  }

  put(key, value) {
    deepKeyJson(this.json, key, value);
    return deepKeyJson(this.json, key);
  }

  me() {
    return {
      json: this.json,
      string: serialize(this.json)
    };
  }

} // class JsonD

export default JsonD;
