const store = {};

store.set = (key, value) => {
  if (value === undefined) return store.remove(key);
	localStorage.setItem(key, store.serialize(value));
	return value;
};

store.get = (key) => {
	let value = store.deserialize(localStorage.getItem(key));
	return (value === undefined ? false : value);
};

store.has = (key) => localStorage.get(key) !== undefined;

store.remove = (key) => localStorage.removeItem(key);

store.clear = () => localStorage.clear();

store.getAll = () => {
  let tempObject = {};
  Object.keys(localStorage).forEach((key, value) => {
  	tempObject[key] = store.deserialize(localStorage[key]);
  });
  return tempObject;
};

store.setAll = (object, prepend) => {
  Object.keys(object).forEach((key) => {
    store.set(
      (prepend ? prepend + '_' : '') + key,
      object[key]
    );
  });
};

store.serialize = (value) => {
  return JSON.stringify(value);
};

store.deserialize = (value) => {
  if (typeof value != 'string') return undefined;
  try { return JSON.parse(value); }
  catch(error) { return value || undefined; }
};

export default store;
