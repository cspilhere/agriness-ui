export const permissionsMatrix = (hasGrantToBasicEntries) => {
  return [{
    description: 'Criação de novos usuários para acesso a granja',
    owner: true,
    administrator: true,
    defaultUser: false,
    viewer: false,
    disableChoose: true
  }, {
    description: 'Criação e exclusão de granja',
    owner: true,
    administrator: false,
    defaultUser: false,
    viewer: false,
    disableChoose: true
  }, {
    description: 'Alterar configurações de metas/índices',
    owner: true,
    administrator: true,
    defaultUser: false,
    viewer: false,
    disableChoose: true
  }, {
    description: 'Incluir, editar e excluir lançamentos',
    owner: true,
    administrator: true,
    defaultUser: true,
    viewer: false,
    disableChoose: true
  }, {
    description: 'Visualizar dados',
    owner: true,
    administrator: true,
    defaultUser: true,
    viewer: false,
    disableChoose: true
  }, {
    description: 'Alterar os cadastros Básicos',
    owner: true,
    administrator: true,
    defaultUser: hasGrantToBasicEntries,
    viewer: false,
    disableChoose: false
  }];
};
