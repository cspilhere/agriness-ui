export const diagnosticTree = () => {
	return [
	{
		"index": 1,
		"name": "DESMAMADOS/FÊMEA/ANO",
		"value": 29.8,
		"target": 31.22,
		"direction": "down"
	},{
		"index": 2,
		"name": "PARTOS/FÊMEA/ANO",
		"value": 2.43,
		"target": 2.49,
		"direction": "down"
	},{
		"index": 21,
		"name": "MÉDIA DE DESMAMADOS",
		"value": 12.26,
		"target": 12.54,
		"direction": "down"
	},{
		"index": 3,
		"name": "DIAS DE GESTAÇÃO",
		"value": 115.03,
		"target": 114.0,
		"direction": "down"
	},{
		"index": 4,
		"name": "DIAS DE LACTAÇÃO",
		"value": 21.32,
		"target": 21.0,
		"direction": "down"
	},{
		"index": 5,
		"name": "DIAS NÃO PRODUTIVOS",
		"value": 13.97,
		"target": 10.0,
		"direction": "down"
	},{
		"index": 6,
		"name": "INTERVALO DESMAME COBERTURA",
		"value": 4.69,
		"target": 5.0,
		"direction": "up"
	},{
		"index": 7,
		"name": "% REPETIÇÃO DE CIO",
		"value": 7.24,
		"target": 6.0,
		"direction": "down"
	},{
		"index": 8,
		"name": "DNP REPETIÇÃO DE CIO",
		"value": 2.79,
		"target": null,
		"direction": null
	},{
		"index": 9,
		"name": "% ABORTO",
		"value": 0.45,
		"target": 1.5,
		"direction": "up"
	},{
		"index": 10,
		"name": "DNP ABORTO",
		"value": 0.4,
		"target": null,
		"direction": null
	},{
		"index": 11,
		"name": "% FALSA PRENHEZ",
		"value": 0.0,
		"target": null,
		"direction": null
	},{
		"index": 12,
		"name": "DNP FALSA PRENHEZ",
		"value": 0.0,
		"target": null,
		"direction": null
	},{
		"index": 13,
		"name": "% MORTE GESTANTES",
		"value": 0.78,
		"target": null,
		"direction": null
	},{
		"index": 14,
		"name": "DNP MORTE GESTANTES",
		"value": 0.89,
		"target": null,
		"direction": null
	},{
		"index": 15,
		"name": "% DESCARTE GESTANTES",
		"value": 1.42,
		"target": null,
		"direction": null
	},
	{
		"index": 16,
		"name": "DNP DESCARTE GESTANTES",
		"value": 1.37,
		"target": null,
		"direction": null
	},{
		"index": 17,
		"name": "% TAXA DE PARIÇÃO",
		"value": 90.06,
		"target": 90.0,
		"direction": "up"
	},{
		"index": 18,
		"name": "DNP MORTE VAZIA",
		"value": 1.45,
		"target": null,
		"direction": null
	},{
		"index": 19,
		"name": "DNP DESCARTE VAZIA",
		"value": 2.42,
		"target": null,
		"direction": null
	},{
		"index": 20,
		"name": "DNP PARADAS",
		"value": 0.0,
		"target": null,
		"direction": null
	},{
		"index": 22,
		"name": "MÉDIA NASCIDOS VIVOS",
		"value": 13.94,
		"target": 12.0,
		"direction": "up"
	},{
		"index": 23,
		"name": "% NATIMORTOS",
		"value": 3.58,
		"target": 2.0,
		"direction": "down"
	},{
		"index": 24,
		"name": "% MUMIFICADOS",
		"value": 3.48,
		"target": 1.0,
		"direction": "down"
	},{
		"index": 25,
		"name": "MÉDIA NASCIDOS TOTAIS",
		"value": 15,
		"target": 12.36,
		"direction": "up"
	},{
		"index": 26,
		"name": "% MORTE DE LEITÕES",
		"value": 12.03,
		"target": 10.0,
		"direction": "down"
	}];
};