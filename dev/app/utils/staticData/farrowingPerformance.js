export const farrowingPerformance = () => {
	return {
		"farrowings": [{
			"index": 1,
			"index_name": "Previstos",
			"quantity": 624.0,
			"percentage": null,
			"information": "O número de partos previstos é baseado nas coberturas realizadas a 114 dias atrás.",
			"is_important": false
		}, {
			"index": 2,
			"index_name": "Realizados dos previstos",
			"quantity": 307.0,
			"percentage": null,
			"information": null,
			"is_important": false
		}, {
			"index": 3,
			"index_name": "Taxa de parição",
			"quantity": null,
			"percentage": 49.20,
			"information": null,
			"is_important": false
		}, {
			"index": 4,
			"index_name": "Taxa de parição ajustada",
			"quantity": null,
			"percentage": 50.25,
			"information": null,
			"is_important": false
		}, {
			"index": 5,
			"index_name": "Partos do período",
			"quantity": 310.0,
			"percentage": null,
			"information": null,
			"is_important": false
		}, {
			"index": 6,
			"index_name": "Partos/Fêmea/Ano",
			"quantity": 2.27,
			"percentage": null,
			"information": "O índice de Partos/Fêmea/Ano é calculado considerando o período de gestação, os dias de lactação da matriz e os dias produtivos.",
			"is_important": true
		}, {
			"index": 7,
			"index_name": "Vivos/Fêmea/Ano",
			"quantity": 21.45,
			"percentage": null,
			"information": "O índice de Vivos/Fêmea/Ano é calculado considerando os nascidos vivos no período e os Parto/Fêmea/Ano no período.",
			"is_important": true
		}],
		"weanings": [{
			"index": 1,
			"index_name": "Total de desmames",
			"quantity": 314.0,
			"percentage": 100.00,
			"information": null,
			"is_important": false
		}, {
			"index": 2,
			"index_name": "Desmames mãe de leite",
			"quantity": 6.0,
			"percentage": 9.14,
			"information": null,
			"is_important": false
		}, {
			"index": 3,
			"index_name": "Período de lactação (dias)",
			"quantity": 22.13,
			"percentage": null,
			"information": null,
			"is_important": false
		}, {
			"index": 4,
			"index_name": "Desmamados/Fêmea/Ano",
			"quantity": 16.58,
			"percentage": null,
			"information": "O Índice de Desmamados/Fêmea/Ano é calculado considerando os leitões desmamados no período e o Parto/Fêmea/Ano no período.",
			"is_important": true
		}, {
			"index": 5,
			"index_name": "Kg/Desmamados/Fêmea/Ano",
			"quantity": 106.59,
			"percentage": null,
			"information": null,
			"is_important": true
		}],
		"borns": [{
			"index": 1,
			"index_name": "Vivos",
			"quantity": 2928.0,
			"percentage": 89.05,
			"average": 9.45,
			"information": null,
			"is_important": false
		}, {
			"index": 2,
			"index_name": "Natimortos",
			"quantity": 333.0,
			"percentage": 10.13,
			"average": 1.07,
			"information": null,
			"is_important": false
		}, {
			"index": 3,
			"index_name": "Mumificados",
			"quantity": 27.0,
			"percentage": 0.82,
			"average": 0.09,
			"information": null,
			"is_important": false
		}, {
			"index": 4,
			"index_name": "Total",
			"quantity": 3288.0,
			"percentage": 100.00,
			"average": 10.61,
			"information": null,
			"is_important": false
		}, {
			"index": 5,
			"index_name": "Peso (kg)",
			"quantity": 4790.90,
			"percentage": null,
			"average": 1.64,
			"information": null,
			"is_important": false
		}, {
			"index": 6,
			"index_name": "Leitões baixa viabilidade",
			"quantity": 177,
			"percentage": 6.05,
			"average": 0.57,
			"information": null,
			"is_important": false
		}],
		"weaneds": [{
			"index": 1,
			"index_name": "Leitões a desmamar",
			"quantity": 2869.0,
			"percentage": null,
			"average": null,
			"information": null,
			"is_important": false
		}, {
			"index": 2,
			"index_name": "Desmamados relac. desm.",
			"quantity": 2249.0,
			"percentage": 78.39,
			"average": 7.30,
			"information": "O número de leitões desmamados corresponde a quantidade de leitões desmamados nos desmames totais realizados no período.",
			"is_important": false
		}, {
			"index": 3,
			"index_name": "Desmamados no período",
			"quantity": 2249.0,
			"percentage": null,
			"average": null,
			"information": "O número de leitões desmamados no período corresponde a quantidade de leitões desmamados considerando os desmames parciais e totais.",
			"is_important": false
		}, {
			"index": 4,
			"index_name": "Idade média",
			"quantity": null,
			"percentage": null,
			"average": 23.06,
			"information": null,
			"is_important": false
		}, {
			"index": 5,
			"index_name": "Mortes relac. desm.",
			"quantity": 620.0,
			"percentage": 21.61,
			"average": 1.97,
			"information": null,
			"is_important": false
		}, {
			"index": 6,
			"index_name": "Mortes no período",
			"quantity": 606.0,
			"percentage": 20.70,
			"average": null,
			"information": null,
			"is_important": false
		}, {
			"index": 7,
			"index_name": "Peso (kg)",
			"quantity": 14455.50,
			"percentage": null,
			"average": 6.43,
			"information": null,
			"is_important": false
		}, {
			"index": 8,
			"index_name": "Peso dos nascidos (kg)",
			"quantity": null,
			"percentage": null,
			"average": 1.64,
			"information": null,
			"is_important": false
		}, {
			"index": 9,
			"index_name": "G.P.D. (kg)",
			"quantity": null,
			"percentage": null,
			"average": 0.208,
			"information": null,
			"is_important": false
		}],
		"farrowing_complementary_indexes": [{
			"index": 1,
			"index_name": "Ciclo médio",
			"value": 3.19,
			"information": null,
			"is_important": false
		}, {
			"index": 2,
			"index_name": "Média de duração (horas)",
			"value": "00:00",
			"information": null,
			"is_important": false
		}, {
			"index": 3,
			"index_name": "Período de gestação",
			"value": 115.77,
			"information": null,
			"is_important": false
		}, {
			"index": 4,
			"index_name": "Intervalo entre partos",
			"value": 160.83,
			"information": null,
			"is_important": false
		}, {
			"index": 5,
			"index_name": "Peso da matriz (kg)",
			"value": 235.00,
			"information": null,
			"is_important": false
		}, {
			"index": 6,
			"index_name": "% Variação de Peso (Cobertura-Parto)",
			"value": 6.82,
			"information": null,
			"is_important": false
		}],
		"weaning_complementary_indexes": [{
			"index": 1,
			"index_name": "Ciclo médio",
			"value": 3.18,
			"information": null,
			"is_important": false
		}, {
			"index": 2,
			"index_name": "Ração consum. Fêmeas (kg)",
			"value": 0.00,
			"information": null,
			"is_important": false
		}, {
			"index": 3,
			"index_name": "Consumo fêmeas/dia (kg)",
			"value": 0.00,
			"information": null,
			"is_important": false
		}, {
			"index": 4,
			"index_name": "Ração consum. leitões (kg)",
			"value": 0.00,
			"information": null,
			"is_important": false
		}, {
			"index": 5,
			"index_name": "Peso aos 21 dias (kg)",
			"value": 5.75,
			"information": null,
			"is_important": false
		}, {
			"index": 6,
			"index_name": "Peso da matriz (kg)",
			"value": 213.43,
			"information": null,
			"is_important": false
		}, {
			"index": 7,
			"index_name": "% Variação de Peso (Parto-Desmame)",
			"value": -9.18,
			"information": null,
			"is_important": false
		}, {
			"index": 8,
			"index_name": "Acerto de estoque na maternidade",
			"value": -7,
			"information": null,
			"is_important": false
		}]
	};
};