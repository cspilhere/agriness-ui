export const breedingPerformance = () => {
	return {
      "matings": [
      {
        "index": 0,
        "index_name": "I.A.",
        "quantity": 416.0,
        "percentage": 100.00,
        "information": null,
        "isImportant": false
    }, {
        "index": 1,
        "index_name": "Monta natural",
        "quantity": 0.0,
        "percentage": 0.00,
        "information": null,
        "isImportant": false
    }, {
        "index": 2,
        "index_name": "Compra de gestante",
        "quantity": 0.0,
        "percentage": 0.0,
        "information": null,
        "isImportant": false
    }, {
        "index": 3,
        "index_name": "Total",
        "quantity": 416.0,
        "percentage": null,
        "information": null,
        "isImportant": false
    }, {
        "index": 4,
        "index_name": "Coberturas até 7 dias",
        "quantity": 187.0,
        "percentage": 74.21,
        "information": "O percentual é calculado considerando a primeira cobertura da matriz após o desmame.",
        "isImportant": true
    }, {
        "index": 5,
        "index_name": "Coberturas acima 7 dias",
        "quantity": 65.0,
        "percentage": 25.79,
        "information": null,
        "isImportant": false
    }, {
        "index": 6,
        "index_name": "Recoberturas",
        "quantity": 70.0,
        "percentage": 16.83,
        "information": null,
        "isImportant": false
    }, {
        "index": 7,
        "index_name": "Primíparas cobertas",
        "quantity": 94.0,
        "percentage": 22.60,
        "information": null,
        "isImportant": false
    }, {
        "index": 8,
        "index_name": "% Múltiplas montas / I.A.",
        "quantity": null,
        "percentage": 0.48,
        "information": null,
        "isImportant": false
    }, {
        "index": 9,
        "index_name": "Nº de montas / I.A. por cobertura",
        "quantity": 1.00,
        "percentage": null,
        "information": null,
        "isImportant": false
    }],
    "reproductive_loss": [
    {
        "index_name": "Repetição de cio",
        "quantity": 68.0,
        "percentage": 16.35,
        "information": null,
        "isImportant": false
    }, {
        "index_name": "Aborto",
        "quantity": 2.0,
        "percentage": 0.48,
        "information": null,
        "isImportant": false
    }, {
        "index_name": "Falsa prenhez",
        "quantity": 0.0,
        "percentage": 0.0,
        "information": null,
        "isImportant": false
    }, {
        "index_name": "Total",
        "quantity": 70.0,
        "percentage": null,
        "information": null,
        "isImportant": true
    }],
    "intervals": [
    {
        "index_name": "Desmame - Cobertura",
        "days": 11.08,
        "information": "Intervalo entre o desmame e a 1ª cobertura pós-desmame, mesmo que essa cobertura tenha resultado em uma perda reprodutiva.",
        "isImportant": true
    }, {
        "index_name": "Desmame - Prenhez",
        "days": 17.99,
        "information": "Intervalo entre o desmame e a cobertura em que efetivamente a matriz entrou em gestação.",
        "isImportant": true
    }, {
        "index_name": "Entrada - 1ª Cobertura",
        "days": 190.59,
        "information": null,
        "isImportant": false
    }, {
        "index_name": "Idade de 1ª Cobertura",
        "days": 236.35,
        "information": null,
        "isImportant": false
    }],
    "complementary_indexes": [
    {
        "index_name": "Ração consumida (kg)",
        "value": 0.00,
        "information": null,
        "isImportant": false
    }, {
        "index_name": "Consumo fêmea/dia (kg)",
        "value": 0.00,
        "information": null,
        "isImportant": false
    }, {
        "index_name": "Peso da matriz (kg)",
        "value": 220.00,
        "information": null,
        "isImportant": false
    }]
    }
}