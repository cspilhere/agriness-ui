export const timeline = () => {
  return [{
		"index": 1,
		"cycle": 0,
		"elements": [{
			"index": 1,
			"element": "node",
			"type": "productive-event",
			"event_type": "purchase",
			"header": ["Entrada", "2014-02-06"],
			"footer": ["120.0 Kg"],
			"value": null,
			"extra": null,
			"is_projection": false
		}, {
			"index": 2,
			"element": "segment",
			"type": "timeline",
			"event_type": null,
			"header": ["53 dias"],
			"footer": ["Preparação"],
			"value": null,
			"extra": null,
			"is_projection": false
		}, {
			"index": 3,
			"element": "node",
			"type": "productive-event",
			"event_type": "heat",
			"header": ["Cio", "2014-03-31"],
			"footer": null,
			"value": null,
			"extra": null,
			"is_projection": false
		},{
			"index": 4,
			"element": "segment",
			"type": "timeline",
			"event_type": null,
			"header": ["21 dias"],
			"footer": ["Cio"],
			"value": null,
			"extra": null,
			"is_projection": false
		},{
			"index": 5,
			"element": "node",
			"type": "productive-event",
			"event_type": "heat",
			"header": ["Cio","2014-04-21"],
			"footer": null,
			"value": null,
			"extra": null,
			"is_projection": false
		},{
			"index": 6,
			"element": "segment",
			"type": "timeline",
			"event_type": null,
			"header": ["18 dias"],
			"footer": null,
			"value": null,
			"extra": null,
			"is_projection": false
		},{
			"index": 7,
			"element": "node",
			"type": "cutoff",
			"event_type": null,
			"header": ["Fim do ciclo", "2014-05-09"],
			"footer": null,
			"value": null,
			"extra": null,
			"is_projection": false
		}]
	}, {
		"index": 2,
		"cycle": 1,
		"elements": [{
			"index": 1,
			"element": "node",
			"type": "productive-event",
			"event_type": "mating",
			"header": ["Cobertura", "2014-05-09"],
			"footer": null,
			"value": "3",
			"extra": [{"date": "2014-05-09",
								"male":"AM123456",
						"stockmanship": "João Carlos",
						"time": "07:30"},
						{"date": "2014-05-09",
								"male":"AM123456",
						"stockmanship": "João Carlos",
						"time": "17:30"},
						{"date": "2014-05-10",
								"male":"RIT125",
						"stockmanship": "Mariana Pereira",
						"time": "07:30"}
							],
			"is_projection": false
		},{
			"index": 2,
			"element": "segment",
			"type": "timeline",
			"event_type": null,
			"header": ["113 dias"],
			"footer": ["Gestação"],
			"value": null,
			"extra": null,
			"is_projection": false
		},{
			"index": 3,
			"element": "node",
			"type": "productive-event",
			"event_type": "farrowing",
			"header": ["Parto", "2014-08-30"],
			"footer": ["1.21 kg","03:50"],
			"value": "13/0",
			"extra": [{"Low viability": 1,
								"Mummified": 0,
						"Stillborn": 0}],
			"is_projection": false
		},{
			"index": 4,
			"element": "segment",
			"type": "timeline",
			"event_type": null,
			"header": ["27 dias"],
			"footer": ["Lactação","RC: 2", "DO: 0", "MO: 1"],
			"value": null,
			"extra": null,
			"is_projection": false
		},{
			"index": 5,
			"element": "node",
			"type": "productive-event",
			"event_type": "total-weaning",
			"header": ["Desmame", "2014-09-26"],
			"footer": ["6.87 kg"],
			"value": "14",
			"extra": [{"date": "2014-09-26",
								"action": "Recebeu",
						"quantity": 2}],
			"is_projection": false
		},{
			"index": 6,
			"element": "segment",
			"type": "timeline",
			"event_type": null,
			"header": ["4 dias"],
			"footer": null,
			"value": null,
			"extra": null,
			"is_projection": false
		},{
			"index": 7,
			"element": "node",
			"type": "non-productive-event",
			"event_type": "skip-heat",
			"header": ["Salta Cio", "2014-09-30"],
			"footer": null,
			"value": null,
			"extra": null,
			"is_projection": false
		},{
			"index": 8,
			"element": "segment",
			"type": "timeline",
			"event_type": null,
			"header": ["10 dias"],
			"footer": ["IDC"],
			"value": null,
			"extra": null,
			"is_projection": false
		},{
			"index": 9,
			"element": "node",
			"type": "cutoff",
			"event_type": null,
			"header": ["Fim do ciclo", "2014-10-20"],
			"footer": null,
			"value": null,
			"extra": null,
			"is_projection": false
		}]
	}, {
		"index": 3,
		"cycle": 2,
		"elements": [{
			"index": 1,
			"element": "node",
			"type": "productive-event",
			"event_type": "mating",
			"header": ["Cobertura", "2014-10-14"],
			"footer": null,
			"value": "2",
			"extra": [{"date": "2014-10-14",
								"male":"AM123456",
						"stockmanship": "João Carlos",
						"time": "07:30"},
						{"date": "2014-10-14",
								"male":"AM123456",
						"stockmanship": "João Carlos",
						"time": "17:30"}
							],
			"is_projection": false
		},{
			"index": 2,
			"element": "segment",
			"type": "timeline",
			"event_type": null,
			"header": ["113 dias"],
			"footer": ["Gestação"],
			"value": null,
			"extra": null,
			"is_projection": false
		},{
			"index": 3,
			"element": "node",
			"type": "productive-event",
			"event_type": "farrowing",
			"header": ["Parto", "2015-02-10"],
			"footer": ["1.61 kg","03:50"],
			"value": "11/0",
			"extra": null,
			"is_projection": false
		},{
			"index": 4,
			"element": "segment",
			"type": "timeline",
			"event_type": null,
			"header": ["22 dias"],
			"footer": ["Lactação","RC: 3", "DO: 0", "MO: 0"],
			"value": null,
			"extra": null,
			"is_projection": false
		},{
			"index": 5,
			"element": "node",
			"type": "productive-event",
			"event_type": "total-weaning",
			"header": ["Desmame", "2015-03-04"],
			"footer": ["6.01 kg"],
			"value": "14",
			"extra": [{"date": "2015-03-05",
								"action": "Recebeu",
						"quantity": 2},
						{"date": "2015-03-07",
								"action": "Recebeu",
						"quantity": 1}],
			"is_projection": false
		},{
			"index": 6,
			"element": "segment",
			"type": "timeline",
			"event_type": null,
			"header": ["7 dias"],
			"footer": ["IDC"],
			"value": null,
			"extra": null,
			"is_projection": false
		},{
			"index": 7,
			"element": "node",
			"type": "cutoff",
			"event_type": null,
			"header": ["Fim do ciclo", "2015-03-11"],
			"footer": null,
			"value": null,
			"extra": null,
			"is_projection": false
		}]
	},{
		"index": 4,
		"cycle": 3,
		"elements": [{
			"index": 1,
			"element": "node",
			"type": "productive-event",
			"event_type": "mating",
			"header": ["Cobertura", "2015-03-11"],
			"footer": null,
			"value": "2",
			"extra": [{"date": "2015-03-11",
								"male":"AM123456",
						"stockmanship": "João Carlos",
						"time": "17:30"},
						{"date": "2015-03-12",
								"male":"AM123456",
						"stockmanship": "João Carlos",
						"time": "07:30"}
							],
			"is_projection": false
		},{
			"index": 2,
			"element": "segment",
			"type": "timeline",
			"event_type": null,
			"header": ["112 dias"],
			"footer": ["Gestação"],
			"value": null,
			"extra": null,
			"is_projection": false
		},{
			"index": 3,
			"element": "node",
			"type": "productive-event",
			"event_type": "farrowing",
			"header": ["Parto", "2015-07-01"],
			"footer": ["1.24 kg","03:10"],
			"value": "15/3",
			"extra": [{"Low viability": 0,
								"Mummified": 1,
						"Stillborn": 2}],
			"is_projection": false
		},{
			"index": 4,
			"element": "segment",
			"type": "timeline",
			"event_type": null,
			"header": ["23 dias"],
			"footer": ["Lactação","RC: 0", "DO: 2", "MO: 0"],
			"value": null,
			"extra": null,
			"is_projection": false
		},{
			"index": 5,
			"element": "node",
			"type": "productive-event",
			"event_type": "total-weaning",
			"header": ["Desmame", "2015-07-24"],
			"footer": ["6.86 kg"],
			"value": "13",
			"extra": [{"date": "2015-07-24",
								"action": "Doou",
						"quantity": 2}],
			"is_projection": false
		},{
			"index": 6,
			"element": "segment",
			"type": "timeline",
			"event_type": null,
			"header": ["26 dias"],
			"footer": ["Mãe de leite"],
			"value": null,
			"extra": null,
			"is_projection": false
		},{
			"index": 7,
			"element": "node",
			"type": "productive-event",
			"event_type": "total-weaning",
			"header": ["Desmame", "2015-08-22"],
			"footer": ["6.34 kg"],
			"value": "11",
			"extra": null,
			"is_projection": false
		},{
			"index": 8,
			"element": "segment",
			"type": "timeline",
			"event_type": null,
			"header": ["26 dias"],
			"footer": ["IDC"],
			"value": null,
			"extra": null,
			"is_projection": false
		},{
			"index": 9,
			"element": "node",
			"type": "cutoff",
			"event_type": null,
			"header": ["Fim do ciclo", "2015-09-15"],
			"footer": null,
			"value": null,
			"extra": null,
			"is_projection": false
		}]
	},{
		"index": 5,
		"cycle": 4,
		"elements": [{
			"index": 1,
			"element": "node",
			"type": "productive-event",
			"event_type": "mating",
			"header": ["Cobertura", "2015-09-15"],
			"footer": null,
			"value": "1",
			"extra": [{"date": "2015-09-15",
								"male":"AM123456",
						"stockmanship": "João Carlos",
						"time": "17:30"}],
			"is_projection": false
		},{
			"index": 2,
			"element": "segment",
			"type": "timeline",
			"event_type": null,
			"header": null,
			"footer": null,
			"value": null,
			"extra": null,
			"is_projection": true
		},{
			"index": 3,
			"element": "node",
			"type": "non-productive-event",
			"event_type": "heat-repetition",
			"header": ["1ª Rep Cio", "2015-10-06"],
			"footer": null,
			"value": null,
			"extra": null,
			"is_projection": true
		},{
			"index": 4,
			"element": "segment",
			"type": "timeline",
			"event_type": null,
			"header": null,
			"footer": null,
			"value": null,
			"extra": null,
			"is_projection": true
		},{
			"index": 5,
			"element": "node",
			"type": "non-productive-event",
			"event_type": "heat-repetition",
			"header": ["2ª Rep Cio", "2015-10-27"],
			"footer": null,
			"value": null,
			"extra": null,
			"is_projection": true
		},{
			"index": 6,
			"element": "segment",
			"type": "timeline",
			"event_type": null,
			"header": null,
			"footer": null,
			"value": null,
			"extra": null,
			"is_projection": true
		},{
			"index": 7,
			"element": "node",
			"type": "productive-event",
			"event_type": "farrowing",
			"header": ["Parto", "2016-01-07"],
			"footer": null,
			"value": null,
			"extra": null,
			"is_projection": true
		}]
	}];
};
