export const indices = () => {
  return {
		"primary_identification": "M1234XYZ",
		"location": "Sala Sul",
		"pen": "B02",
		"animal_status":  "Ativo",
		"cycles": 7,
		"matings": 2,
		"reproductive_stage": "Gestante",
		"birthdate": "2013-09-18",
		"incoming_date": "2014-02-06",
		"first_mating_date": "2014-05-09",
		"first_mating_age": 233,
		"crossbred": "CB",
		"entity": "João Amaro dos Santos Ltda",
		"ranking": 20,
		"average_alive": 0,
		"average_weanings": 0,
		"performance_indicators": {
		"pigs_per_sow_per_year": {
			"value": 23.38,
			"target": 28.0,
			"direction": "down"
		},
		"farrowing_per_sow_per_year": {
			"value": 2.35,
			"target": 2.45,
			"direction": "down"
		},
		"non_productive_sow_days": {
			"value": 12.6,
			"target": 12.0,
			"direction": "down"
		},
		"gestation_days": {
			"value": 113.0,
			"target": 114.00,
			"direction": "up"
		},
		"lactation_days": {
			"value": 29.8,
			"target": 24.0,
			"direction": "down"
		},
		"live_born_pigs": {
			"value": 13.2,
			"target": 13.9,
			"direction": "down"
		},
		"number_weaned": {
			"value": 14.2,
			"target": 12.9,
			"direction": "up"
		},
		"birth_weight": {
			"value": 1.37,
			"target": 1.40,
			"direction": "down"
		},
		"litter_weaning_weight": {
		"value": 6.27,
		"target": 6.25,
		"direction": "up"
		},
		"weaning_to_mating": {
		"value": 4.33,
		"target": 6.0,
		"direction": "up"
		},
		"stillborn": {
		"value": 4.55,
		"target": 5.0,
		"direction": "up"
		},
		"mummified": {
		"value": 7.58,
		"target": 2.8,
		"direction": "down"
		}}
	};
};
