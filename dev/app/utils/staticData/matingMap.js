export const matingMap = () => {
	return {
		"targets": {
			"matings": 21,
			"farrowing_rate": 85.00,
			"mortality_in_farrowing": 9.09,
			"mortality_in_nursery": 1.50,
			"mortality_in_finishing": 1.5
		},
		"details": [{
			"mating_details": {
				"week_informations": {
					"week": 36,
					"first_day_of_week": "SEG",
					"first_date_of_week": "2016-08-29",
					"matings_quantity": 25,
					"matings_percentage": 119.0
				},
				"events_by_week_totals": [{
					"gestation_week": 1,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 25,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 2,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 25,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 3,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 25,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 4,
					"heat_repetitions": {
						"total": 1,
						"extra": [{
							"primary_identification": "1319",
							"gestation_days": 24,
							"date": "2016-09-22"
						}]
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 24,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 5,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 24,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 6,
					"heat_repetitions": {
						"total": 1,
						"extra": [{
							"primary_identification": "111",
							"gestation_days": 38,
							"date": "2016-10-06"
						}]
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 23,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 7,
					"heat_repetitions": {
						"total": 1,
						"extra": [{
							"primary_identification": "AM123",
							"gestation_days": 45,
							"date": "2016-10-13"
						}]
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 22,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 8,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 1,
						"extra": [{
							"primary_identification": "AM126",
							"gestation_days": 52,
							"date": "2016-10-20"
						}]
					},
					"pregnants": 21,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 9,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 21,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 10,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 21,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 11,
					"heat_repetitions": {
						"total": 1,
						"extra": [{
							"primary_identification": "AM127",
							"gestation_days": 52,
							"date": "2016-11-14"
						}]
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 20,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 12,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 20,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 13,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 20,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 14,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 20,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 15,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 20,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 16,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 200,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}],
				"totals": {
					"pregnants_in_last_week": 20,
					"heat_repetitions_of_all_weeks": 4,
					"abortions_of_all_weeks": 1,
					"fake_pregnancies_of_all_weeks": 0,
					"deaths_of_all_weeks": 0,
					"culleds_of_all_weeks": 0
				}
			},
			"farrowing_details": {
				"week": 52,
				"average_date": "2016-12-22",
				"piglets_total": 204,
				"total": 18,
				"rate": 72.0,
				"is_projection": false
			},
			"weaning_details": {
				"week": 2,
				"average_date": "2017-01-14",
				"piglets_total": 185,
				"total": 18,
				"piglets_death_percentage": -9.07,
				"is_projection": false
			},
			"nursery_output_details": {
				"week": 10,
				"average_date": "2017-03-09",
				"piglets_total": 182
			},
			"finishing_output_details": {
				"week": 22,
				"average_date": "2017-06-01",
				"piglets_total": 179
			}
		}, {
			"mating_details": {
				"week_informations": {
					"week": 37,
					"first_day_of_week": "SEG",
					"first_date_of_week": "2016-09-05",
					"matings_quantity": 22,
					"matings_percentage": 104.8
				},
				"events_by_week_totals": [{
					"gestation_week": 1,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 22,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 2,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 22,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 3,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 23,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 4,
					"heat_repetitions": {
						"total": 3,
						"extra": [{
							"primary_identification": "TA123456",
							"gestation_days": 26,
							"date": "2016-10-01"
						}, {
							"primary_identification": "409",
							"gestation_days": 27,
							"date": "2016-10-02"
						}, {
							"primary_identification": "25698",
							"gestation_days": 27,
							"date": "2016-10-02"
						}]
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 19,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 5,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 18,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 1,
						"extra": [{
							"primary_identification": "TA123456",
							"gestation_days": 26,
							"date": "2016-10-01"
						}]
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 6,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 18,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 7,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 18,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 8,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 18,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 9,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 18,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 10,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 18,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 11,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 18,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 12,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 18,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 13,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 18,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 14,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 18,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 15,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 17,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 1,
						"extra": [{
							"primary_identification": "1389",
							"gestation_days": 24,
							"date": "2016-12-14"
						}]
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 16,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 17,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}],
				"totals": {
					"pregnants_in_last_week": 17,
					"heat_repetitions_of_all_weeks": 3,
					"abortions_of_all_weeks": 0,
					"fake_pregnancies_of_all_weeks": 0,
					"deaths_of_all_weeks": 0,
					"culleds_of_all_weeks": 2
				}
			},
			"farrowing_details": {
				"week": 53,
				"average_date": "2016-12-29",
				"piglets_total": 187,
				"total": 17,
				"rate": 77.2,
				"is_projection": false
			},
			"weaning_details": {
				"week": 3,
				"average_date": "2017-01-21",
				"piglets_total": 170,
				"total": 17,
				"piglets_death_percentage": null,
				"is_projection": true
			},
			"nursery_output_details": {
				"week": 11,
				"average_date": "2017-03-16",
				"piglets_total": 167
			},
			"finishing_output_details": {
				"week": 23,
				"average_date": "2017-06-08",
				"piglets_total": 164
			}
		}, {
			"mating_details": {
				"week_informations": {
					"week": 38,
					"first_day_of_week": "SEG",
					"first_date_of_week": "2016-09-12",
					"matings_quantity": 23,
					"matings_percentage": 109.5
				},
				"events_by_week_totals": [{
					"gestation_week": 1,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 23,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 2,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 23,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 3,
					"heat_repetitions": {
						"total": 1,
						"extra": [{
							"primary_identification": "1392",
							"gestation_days": 21,
							"date": "2016-10-03"
						}]
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 21,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 1,
						"extra": [{
							"primary_identification": "1390",
							"gestation_days": 18,
							"date": "2016-09-30"
						}]
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 4,
					"heat_repetitions": {
						"total": 3,
						"extra": null
					},
					"abortions": {
						"total": 1,
						"extra": [{
							"primary_identification": "MA45623",
							"gestation_days": 28,
							"date": "2016-10-10"
						}]
					},
					"pregnants": 20,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 5,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 20,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 6,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 20,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 7,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 20,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 8,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 20,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 9,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 19,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 1,
						"extra": [{
							"primary_identification": "468912",
							"gestation_days": 62,
							"date": "2016-11-13"
						}]
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 10,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 19,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 11,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 19,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 12,
					"heat_repetitions": {
						"total": 1,
						"extra": [{
							"primary_identification": "178523",
							"gestation_days": 87,
							"date": "2016-12-08"
						}]
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 18,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 13,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 18,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 14,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 18,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 15,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 18,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 16,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 18,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}],
				"totals": {
					"pregnants_in_last_week": 18,
					"heat_repetitions_of_all_weeks": 3,
					"abortions_of_all_weeks": 0,
					"fake_pregnancies_of_all_weeks": 0,
					"deaths_of_all_weeks": 0,
					"culleds_of_all_weeks": 3
				}
			},
			"farrowing_details": {
				"week": 1,
				"average_date": "2017-01-05",
				"piglets_total": 198,
				"total": 18,
				"rate": 78.26,
				"is_projection": false
			},
			"weaning_details": {
				"week": 4,
				"average_date": "2017-01-28",
				"piglets_total": 177,
				"total": 18,
				"piglets_death_percentage": null,
				"is_projection": true
			},
			"nursery_output_details": {
				"week": 12,
				"average_date": "2017-03-23",
				"piglets_total": 177
			},
			"finishing_output_details": {
				"week": 24,
				"average_date": "2017-06-15",
				"piglets_total": 174
			}
		}, {
			"mating_details": {
				"week_informations": {
					"week": 39,
					"first_day_of_week": "SEG",
					"first_date_of_week": "2016-09-19",
					"matings_quantity": 24,
					"matings_percentage": 114.3
				},
				"events_by_week_totals": [{
					"gestation_week": 1,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 24,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 2,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 24,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 3,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 24,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 4,
					"heat_repetitions": {
						"total": 2,
						"extra": [{
								"primary_identification": "6952145",
								"gestation_days": 28,
								"date": "2016-10-17"
							},
							{
								"primary_identification": "AT125",
								"gestation_days": 28,
								"date": "2016-10-17"
							}
						]
					},
					"abortions": {
						"total": 1,
						"extra": [{
							"primary_identification": "CC22",
							"gestation_days": 29,
							"date": "2016-10-18"
						}]
					},
					"pregnants": 21,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 5,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 21,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 6,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 21,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 7,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 21,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 8,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 21,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 9,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 21,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 10,
					"heat_repetitions": {
						"total": 1,
						"extra": [{
							"primary_identification": "TT1389",
							"gestation_days": 54,
							"date": "2016-11-12"
						}]
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 20,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 11,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 20,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 12,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 20,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 13,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 20,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 14,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 20,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 15,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 20,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 16,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 20,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}],
				"totals": {
					"pregnants_in_last_week": 20,
					"heat_repetitions_of_all_weeks": 3,
					"abortions_of_all_weeks": 1,
					"fake_pregnancies_of_all_weeks": 0,
					"deaths_of_all_weeks": 0,
					"culleds_of_all_weeks": 0
				}
			},
			"farrowing_details": {
				"week": 2,
				"average_date": "2017-01-12",
				"piglets_total": 220,
				"total": 20,
				"rate": 83.33,
				"is_projection": false
			},
			"weaning_details": {
				"week": 5,
				"average_date": "2017-02-04",
				"piglets_total": 200,
				"total": 20,
				"piglets_death_percentage": null,
				"is_projection": true
			},
			"nursery_output_details": {
				"week": 13,
				"average_date": "2017-03-30",
				"piglets_total": 197
			},
			"finishing_output_details": {
				"week": 25,
				"average_date": "2017-06-22",
				"piglets_total": 194
			}
		}, {
			"mating_details": {
				"week_informations": {
					"week": 40,
					"first_day_of_week": "SEG",
					"first_date_of_week": "2016-09-26",
					"matings_quantity": 22,
					"matings_percentage": 104.8
				},
				"events_by_week_totals": [{
					"gestation_week": 1,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 22,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 2,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 22,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 3,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 22,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 4,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 22,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 5,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 22,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 6,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 22,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 7,
					"heat_repetitions": {
						"total": 1,
						"extra": [{
							"primary_identification": "215782",
							"gestation_days": 43,
							"date": "2016-11-08"
						}]
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 20,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 1,
						"extra": [{
							"primary_identification": "TAS12",
							"gestation_days": 24,
							"date": "2016-11-14"
						}]
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 8,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 20,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 9,
					"heat_repetitions": {
						"total": 1,
						"extra": [{
							"primary_identification": "S1389",
							"gestation_days": 24,
							"date": "2016-11-25"
						}]
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 19,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 10,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 19,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 11,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 19,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 12,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 19,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 13,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 19,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 14,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 19,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 15,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 19,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 16,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 19,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}],
				"totals": {
					"pregnants_in_last_week": 19,
					"heat_repetitions_of_all_weeks": 2,
					"abortions_of_all_weeks": 0,
					"fake_pregnancies_of_all_weeks": 0,
					"deaths_of_all_weeks": 0,
					"culleds_of_all_weeks": 1
				}
			},
			"farrowing_details": {
				"week": 3,
				"average_date": "2017-01-18",
				"piglets_total": 209,
				"total": 19,
				"rate": 86.36,
				"is_projection": false
			},
			"weaning_details": {
				"week": 6,
				"average_date": "2017-02-10",
				"piglets_total": 190,
				"total": 19,
				"piglets_death_percentage": null,
				"is_projection": true
			},
			"nursery_output_details": {
				"week": 14,
				"average_date": "2017-04-05",
				"piglets_total": 187
			},
			"finishing_output_details": {
				"week": 26,
				"average_date": "2017-06-28",
				"piglets_total": 184
			}
		}, {
			"mating_details": {
				"week_informations": {
					"week": 41,
					"first_day_of_week": "SEG",
					"first_date_of_week": "2016-10-03",
					"matings_quantity": 19,
					"matings_percentage": 90.5
				},
				"events_by_week_totals": [{
					"gestation_week": 1,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 19,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 2,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 19,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 3,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 19,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 4,
					"heat_repetitions": {
						"total": 2,
						"extra": [{
							"primary_identification": "1389",
							"gestation_days": 23,
							"date": "2016-10-26"
						}, {
							"primary_identification": "1389",
							"gestation_days": 24,
							"date": "2016-10-27"
						}]
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 17,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 5,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 17,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 6,
					"heat_repetitions": {
						"total": 1,
						"extra": [{
							"primary_identification": "1389",
							"gestation_days": 37,
							"date": "2016-11-09"
						}]
					},
					"abortions": {
						"total": 1,
						"extra": [{
							"primary_identification": "A14529",
							"gestation_days": 37,
							"date": "2016-11-09"
						}]
					},
					"pregnants": 15,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 7,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 15,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 8,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 15,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 9,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 15,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 10,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 15,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 11,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 15,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 12,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 15,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 13,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 15,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 14,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 15,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 15,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 15,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 16,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 15,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}],
				"totals": {
					"pregnants_in_last_week": 15,
					"heat_repetitions_of_all_weeks": 3,
					"abortions_of_all_weeks": 1,
					"fake_pregnancies_of_all_weeks": 0,
					"deaths_of_all_weeks": 0,
					"culleds_of_all_weeks": 0
				}
			},
			"farrowing_details": {
				"week": 4,
				"average_date": "2017-01-25",
				"piglets_total": 165,
				"total": 15,
				"rate": 78.95,
				"is_projection": false
			},
			"weaning_details": {
				"week": 7,
				"average_date": "2017-02-17",
				"piglets_total": 150,
				"total": 15,
				"piglets_death_percentage": null,
				"is_projection": true
			},
			"nursery_output_details": {
				"week": 15,
				"average_date": "2017-04-12",
				"piglets_total": 147
			},
			"finishing_output_details": {
				"week": 27,
				"average_date": "2017-07-05",
				"piglets_total": 144
			}
		}, {
			"mating_details": {
				"week_informations": {
					"week": 42,
					"first_day_of_week": "SEG",
					"first_date_of_week": "2016-10-10",
					"matings_quantity": 25,
					"matings_percentage": 119.0
				},
				"events_by_week_totals": [{
					"gestation_week": 1,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 25,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 2,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 25,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 3,
					"heat_repetitions": {
						"total": 1,
						"extra": [{
							"primary_identification": "1389",
							"gestation_days": 21,
							"date": "2016-10-31"
						}]
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 24,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 4,
					"heat_repetitions": {
						"total": 1,
						"extra": [{
							"primary_identification": "1389",
							"gestation_days": 23,
							"date": "2016-11-02"
						}]
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 23,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 5,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 23,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 6,
					"heat_repetitions": {
						"total": 1,
						"extra": [{
							"primary_identification": "1389",
							"gestation_days": 37,
							"date": "2016-11-16"
						}]
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 22,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 7,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 22,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 8,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 22,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 9,
					"heat_repetitions": {
						"total": 1,
						"extra": [{
							"primary_identification": "1389",
							"gestation_days": 59,
							"date": "2016-12-08"
						}]
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 21,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 10,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 21,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 11,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 21,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 12,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 21,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 13,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 21,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 14,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 21,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 15,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 21,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}],
				"totals": {
					"pregnants_in_last_week": 21,
					"heat_repetitions_of_all_weeks": 4,
					"abortions_of_all_weeks": 0,
					"fake_pregnancies_of_all_weeks": 0,
					"deaths_of_all_weeks": 0,
					"culleds_of_all_weeks": 0
				}
			},
			"farrowing_details": {
				"week": 5,
				"average_date": "2017-02-02",
				"piglets_total": 231,
				"total": 21,
				"rate": 84.00,
				"is_projection": true
			},
			"weaning_details": {
				"week": 8,
				"average_date": "2017-02-25",
				"piglets_total": 210,
				"total": 21,
				"piglets_death_percentage": null,
				"is_projection": true
			},
			"nursery_output_details": {
				"week": 16,
				"average_date": "2017-04-20",
				"piglets_total": 206
			},
			"finishing_output_details": {
				"week": 28,
				"average_date": "2017-07-13",
				"piglets_total": 202
			}
		}, {
			"mating_details": {
				"week_informations": {
					"week": 43,
					"first_day_of_week": "SEG",
					"first_date_of_week": "2016-10-17",
					"matings_quantity": 24,
					"matings_percentage": 114.3
				},
				"events_by_week_totals": [{
					"gestation_week": 1,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 24,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 2,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 24,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 3,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 24,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 4,
					"heat_repetitions": {
						"total": 2,
						"extra": [{
							"primary_identification": "1389",
							"gestation_days": 23,
							"date": "2016-11-09"
						}, {
							"primary_identification": "1389",
							"gestation_days": 24,
							"date": "2016-11-10"
						}]
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 22,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 5,
					"heat_repetitions": {
						"total": 1,
						"extra": [{
							"primary_identification": "1389",
							"gestation_days": 32,
							"date": "2016-11-18"
						}]
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 21,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 6,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 21,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 7,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 21,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 8,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 21,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 9,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 21,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 10,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 21,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 11,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 21,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 12,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 21,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 13,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 21,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 14,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 21,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}],
				"totals": {
					"pregnants_in_last_week": 21,
					"heat_repetitions_of_all_weeks": 3,
					"abortions_of_all_weeks": 0,
					"fake_pregnancies_of_all_weeks": 0,
					"deaths_of_all_weeks": 0,
					"culleds_of_all_weeks": 0
				}
			},
			"farrowing_details": {
				"week": 6,
				"average_date": "2017-02-09",
				"piglets_total": 231,
				"total": 21,
				"rate": 87.5,
				"is_projection": true
			},
			"weaning_details": {
				"week": 9,
				"average_date": "2017-03-04",
				"piglets_total": 210,
				"total": 21,
				"piglets_death_percentage": null,
				"is_projection": true
			},
			"nursery_output_details": {
				"week": 17,
				"average_date": "2017-04-27",
				"piglets_total": 206
			},
			"finishing_output_details": {
				"week": 29,
				"average_date": "2017-07-20",
				"piglets_total": 202
			}
		}, {
			"mating_details": {
				"week_informations": {
					"week": 44,
					"first_day_of_week": "SEG",
					"first_date_of_week": "2016-10-24",
					"matings_quantity": 24,
					"matings_percentage": 114.3
				},
				"events_by_week_totals": [{
					"gestation_week": 1,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 24,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 2,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 24,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 3,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 24,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 4,
					"heat_repetitions": {
						"total": 1,
						"extra": [{
							"primary_identification": "1389",
							"gestation_days": 23,
							"date": "2016-11-16"
						}]
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 23,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 5,
					"heat_repetitions": {
						"total": 1,
						"extra": [{
							"primary_identification": "1389",
							"gestation_days": 32,
							"date": "2016-11-25"
						}]
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 22,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 6,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 22,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 7,
					"heat_repetitions": {
						"total": 2,
						"extra": [{
							"primary_identification": "1389",
							"gestation_days": 44,
							"date": "2016-12-07"
						}, {
							"primary_identification": "1322",
							"gestation_days": 46,
							"date": "2016-12-09"
						}]
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 20,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 8,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 20,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 9,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 20,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 10,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 20,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 11,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 20,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 12,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 20,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 13,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 20,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}],
				"totals": {
					"pregnants_in_last_week": 20,
					"heat_repetitions_of_all_weeks": 4,
					"abortions_of_all_weeks": 0,
					"fake_pregnancies_of_all_weeks": 0,
					"deaths_of_all_weeks": 0,
					"culleds_of_all_weeks": 0
				}
			},
			"farrowing_details": {
				"week": 7,
				"average_date": "2017-02-15",
				"piglets_total": 220,
				"total": 20,
				"rate": 83.33,
				"is_projection": true
			},
			"weaning_details": {
				"week": 10,
				"average_date": "2017-03-10",
				"piglets_total": 200,
				"total": 20,
				"piglets_death_percentage": null,
				"is_projection": true
			},
			"nursery_output_details": {
				"week": 18,
				"average_date": "2017-05-03",
				"piglets_total": 197
			},
			"finishing_output_details": {
				"week": 30,
				"average_date": "2017-07-26",
				"piglets_total": 194
			}
		}, {
			"mating_details": {
				"week_informations": {
					"week": 45,
					"first_day_of_week": "SEG",
					"first_date_of_week": "2016-10-31",
					"matings_quantity": 16,
					"matings_percentage": 76.2
				},
				"events_by_week_totals": [{
					"gestation_week": 1,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 16,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 2,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 16,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 3,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 16,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 4,
					"heat_repetitions": {
						"total": 1,
						"extra": [{
							"primary_identification": "1389",
							"gestation_days": 24,
							"date": "2016-11-24"
						}]
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 15,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 5,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 15,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 6,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 15,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 7,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 15,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 8,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 15,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 9,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 15,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 10,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 15,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 11,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 15,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}, {
					"gestation_week": 12,
					"heat_repetitions": {
						"total": 0,
						"extra": null
					},
					"abortions": {
						"total": 0,
						"extra": null
					},
					"pregnants": 15,
					"deaths": {
						"total": 0,
						"extra": null
					},
					"culleds": {
						"total": 0,
						"extra": null
					},
					"fake_pregnancies": {
						"total": 0,
						"extra": null
					}
				}],
				"totals": {
					"pregnants_in_last_week": 15,
					"heat_repetitions_of_all_weeks": 1,
					"abortions_of_all_weeks": 0,
					"fake_pregnancies_of_all_weeks": 0,
					"deaths_of_all_weeks": 0,
					"culleds_of_all_weeks": 0
				}
			},
			"farrowing_details": {
				"week": 8,
				"average_date": "2017-02-23",
				"piglets_total": 165,
				"total": 15,
				"rate": 93.75,
				"is_projection": true
			},
			"weaning_details": {
				"week": 11,
				"average_date": "2017-03-18",
				"piglets_total": 150,
				"total": 15,
				"piglets_death_percentage": null,
				"is_projection": true
			},
			"nursery_output_details": {
				"week": 19,
				"average_date": "2017-05-11",
				"piglets_total": 147
			},
			"finishing_output_details": {
				"week": 31,
				"average_date": "2017-08-03",
				"piglets_total": 144
			}
		}]
	};
};