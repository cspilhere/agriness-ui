export const farmReports = () => {
  return {
    "count": undefined,
    "data": [
      {
        "name": "Performance",
        "reports": [
          {
            "internal_name": "stocks",
            "name": "Estoques",
            "note": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonumm lorem.",
            "tags": []  
          },
          {
            "internal_name": "S3 geral",
            "name": "general",
            "note": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonumm lorem.",
            "tags": []  
          }
        ]
      },
      {
        "name": "Listagem",
        "reports": [
          {
            "internal_name": "stocks",
            "name": "Estoques",
            "note": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonumm lorem.",
            "tags": []  
          },
          {
            "internal_name": "S3 geral",
            "name": "general",
            "note": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonumm lorem.",
            "tags": []  
          }
        ]
      }
    ]
  }
}