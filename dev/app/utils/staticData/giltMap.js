export const giltMap = () => {
    return [
    {
        "week": 18,
        "sows": [{
            "id": 45,
            "primary_identification": "AM1554",
            "input_age": 184,
            "first_heat_age": 214,
            "heats": [{
                "index": 1,
                "date": "2016-06-04"
            }, {
                "index": 2,
                "date": "2016-06-23"
            }],
            "next_heat_date": "2016-07-14",
            "first_mating_date": "2016-07-16",
            "age": 235,
            "days_between_input_and_first_heat": 30,
            "days_between_first_heat_and_mating": 42,
            "days_between_input_and_mating": 72,
            "piglets_live_born_total": 16,
            "piglets_born_total": 18,
            "piglets_average_weight": 1.6,
            "piglets_weaned_total": 14,
            "extra": null
        }, {
            "id": 59,
            "primary_identification": "AM1660",
            "input_age": 166,
            "first_heat_age": 180,
            "heats": [{
                "index": 1,
                "date": "2016-05-19"
            }, {
                "index": 2,
                "date": "2016-06-23"
            }],
            "next_heat_date": "2016-07-14",
            "first_mating_date": "2016-07-16",
            "age": 232,
            "days_between_input_and_first_heat": 14,
            "days_between_first_heat_and_mating": 58,
            "days_between_input_and_mating": 72,
            "piglets_live_born_total": null,
            "piglets_born_total": null,
            "piglets_average_weight": null,
            "piglets_weaned_total": null,
            "extra": {
                "animal_event": {
                    "name": "Morte de fêmea"
                },
                "gestation_days": 25
            }
        }]
    }, {
        "week": 20,
        "sows": [{
            "id": 102,
            "primary_identification": "AM2469",
            "input_age": 170,
            "first_heat_age": 187,
            "heats": [{
                "index": 1,
                "date": "2016-05-30"
            }, {
                "index": 2,
                "date": "2016-06-18"
            }],
            "next_heat_date": "2016-07-09",
            "first_mating_date": null,
            "age": null,
            "days_between_input_and_first_heat": null,
            "days_between_first_heat_and_mating": null,
            "days_between_input_and_mating": null,
            "piglets_live_born_total": null,
            "piglets_born_total": null,
            "piglets_average_weight": null,
            "piglets_weaned_total": null,
            "extra": null
        }, {
            "id": 1,
            "primary_identification": "AM2473",
            "input_age": 157,
            "first_heat_age": 171,
            "heats": [{
                "index": 1,
                "date": "2016-05-27"
            }, {
                "index": 2,
                "date": "2016-06-18"
            }],
            "next_heat_date": "2016-07-09",
            "first_mating_date": "2016-07-09",
            "age": 235,
            "days_between_input_and_first_heat": 17,
            "days_between_first_heat_and_mating": 43,
            "days_between_input_and_mating": 57,
            "piglets_live_born_total": null,
            "piglets_born_total": null,
            "piglets_average_weight": null,
            "piglets_weaned_total": null,
            "extra": null
        }, {
            "id": 5,
            "primary_identification": "AM2474",
            "input_age": 170,
            "first_heat_age": 181,
            "heats": [{
                "index": 1,
                "date": "2016-05-24"
            }, {
                "index": 2,
                "date": "2016-06-16"
            }],
            "next_heat_date": "2016-07-07",
            "first_mating_date": "2016-07-10",
            "age": 235,
            "days_between_input_and_first_heat": 11,
            "days_between_first_heat_and_mating": 47,
            "days_between_input_and_mating": 58,
            "piglets_live_born_total": 15,
            "piglets_born_total": 15,
            "piglets_average_weight": 1.525,
            "piglets_weaned_total": null,
            "extra": null
        }]
    }]
}