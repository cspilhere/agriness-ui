import app from 'app';
import rootController from 'routes/root/root.controller';

app.config((
  $stateProvider,
  $urlRouterProvider,
  appConstants,
  $locationProvider
) => {

  $locationProvider.html5Mode(false);
  $locationProvider.hashPrefix('');

  $stateProvider
  .state('app', {
    url: '/',
    templateUrl: appConstants.BASE_ROUTES + 'root/root.template.html',
    controller: rootController
  });

  $urlRouterProvider.when('/', '/');
  $urlRouterProvider.otherwise('/');

});
