## .section
- .section have the responsability about the main vertical padding
- .section have full width with vertical and horizontal padding
- When .section have no .panel inside, he can use .section--panel-compensation modifier
- .main-footer inherit from .section by default

## .main-header
- .main-header have full width with horizontal padding

## .panel
- .panel have all padding

## .container
- .container is the generic wrapper that keeps the boxed behavior with max-width and no margin or padding

## .grid
- .grid__col has default gutter or a modifier with gutter, depends the main layout rule for grids
