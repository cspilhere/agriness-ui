# Agriness S3 Air


Inventário de componentes

form-component (base)
- O form-component o elemento padrão do form,
- É responsável pela validação inline, e controla os seus filhos para este fim,
- É responsável pelo comportamento inline quando utilizado dentro de um parágrafo,

Elementos do form-component:
  - input-text
  - input-select
  - radio
  - button-radio
  - checkbox

"Os elementos são componentes independentes do form-component mas que fazem parte
da estrutura base do form-component, raramente serão utilizados fora."


Dependências do form-component:
  - dropdown
    - dropdown--lookup
    - dropdown--date

" As dependências são componentes independentes que podem ser utilizados como inputs
assumindo o mesmo comportamento dos inputs padrão para questões de validação inline."


Modificadores do form-component:

  - form-component--light - Limpa o form-component e seus filhos removendo bordas e ajustando o padding para ficar lado a lado de outro form-component.
  - form-component--side-label - Posiciona a label ao lado do input.
  - form-component--inline - Torna o form-component um elemento inline, para ser utilizado dentro de parágrafos, responsabilidade atribuída para manter a estrutura de validação inline.
  - form-component--small, --medium - Ajusta e fixa o tamanho do form-component, responsabilidade atribuída para manter a estrutura de validação inline.




input-text (base)
- componente padrão de formulário, pode ser usado dentro de qualquer componente, mas depende do parentesco com o form-component para ter validação inline.


- Modificadores do input-text

  - input-text--inline - limpa o padding e bordas para ser usado dentro de um parágrafo, atualmente esta responsabilidade está com o form-component
  - input-text--line - Limpa as bordas deixando apenas a borda inferior, mantém as demais propriedades do input-text base




select (base)
- componente padrão, assume as propriedades do input-text e acrescenta uma seta de dropdown no lado direito.
- não possui modificadores.



slider
- Componente range, não precisa ser usado dentro do form-component pois este tipo de componente não necessita validação.




input-radio (base)
- Componente radio, comportamento padrão.




button-radio (base)
- Componente radio, comportamento padrão com a aparência de botão.
- Extende as classes padrão de botão (.button e .button__label).



checkbox (base)
- Componente radio, comportamento padrão.

Modificadores do checkbox

  - checkbox--secondary - Muda o border radius para 100% e aumenta para 20px o
  tamanho dos lados do component.
  - checkbox switch (checkbox__token--switch)- Muda o formato do token para
  switch reposiciona as labels.
    - o checkbox switch depende de uma estrutura diferenciada para ter o
    comportamento desejado.
  - checkbox--icon-toggle - Transforma o checkbox em um toggle de ícone, para
  ser utilizado especialmente junto ao form-component__label
  (ex. cadeado para fixar campo)
  - checkbox--icon-fav - Transforma o checkbox em um toggle de ícone grande,
  geralmente utilizado fora do form-component (ex. coração de favorito)


input-file (base)
-Componente padrão de upload de arquivos, depende de uma estrutura padrão
para ter a aparência e comportamento desejados.
